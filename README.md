# YSWebViewProvider

[![CI Status](http://img.shields.io/travis/478356182@qq.com/YSWebViewProvider.svg?style=flat)](https://travis-ci.org/478356182@qq.com/YSWebViewProvider)
[![Version](https://img.shields.io/cocoapods/v/YSWebViewProvider.svg?style=flat)](http://cocoapods.org/pods/YSWebViewProvider)
[![License](https://img.shields.io/cocoapods/l/YSWebViewProvider.svg?style=flat)](http://cocoapods.org/pods/YSWebViewProvider)
[![Platform](https://img.shields.io/cocoapods/p/YSWebViewProvider.svg?style=flat)](http://cocoapods.org/pods/YSWebViewProvider)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

YSWebViewProvider is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'YSWebViewProvider'
```

## Author

478356182@qq.com, 478356182@qq.com

## License

YSWebViewProvider is available under the MIT license. See the LICENSE file for more info.
