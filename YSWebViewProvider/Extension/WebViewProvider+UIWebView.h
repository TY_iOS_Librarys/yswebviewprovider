//
//  WebViewProvider+UIWebView.h
//  PlayApp
//
//  Created by yans on 2017/1/19.
//  Copyright © 2017年 hzty. All rights reserved.
//

#import "WebViewProvider.h"

@interface WebViewProvider (UIWebView)

#pragma mark - UIWebView Delegate Methods

@property (nonatomic, copy) BOOL(^shouldStartLoadWithRequest)(UIWebView *,NSURLRequest *,UIWebViewNavigationType);
@property (nonatomic, copy) void(^webViewDidStartLoad)(UIWebView *);
@property (nonatomic, copy) void(^didFailLoadWithError)(UIWebView *,NSError *);
@property (nonatomic, copy) void(^webViewDidFinishLoad)(UIWebView *);

- (id)createUIWebViewWithFrame:(CGRect)frame;

@end
