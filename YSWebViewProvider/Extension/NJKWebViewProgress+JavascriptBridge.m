//
//  NJKWebViewProgress+JavascriptBridge.m
//  PlayApp
//
//  Created by yans on 2017/1/19.
//  Copyright © 2017年 hzty. All rights reserved.
//

#import "NJKWebViewProgress+JavascriptBridge.h"
#import <objc/runtime.h>

@implementation NJKWebViewProgress (JavascriptBridge)

- (id)webBridgeDelegate {
    return objc_getAssociatedObject(self, _cmd);
}

- (void)setWebBridgeDelegate:(id)webBridgeDelegate {
    objc_setAssociatedObject(self, @selector(webBridgeDelegate), webBridgeDelegate, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}



#pragma mark -
#pragma mark UIWebViewDelegate

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSString *completeRPCURL = @"webviewprogressproxy:///complete";
    
    if ([request.URL.absoluteString isEqualToString:completeRPCURL]) {
        [self performSelector:@selector(completeProgress)];
        return NO;
    }
    
    BOOL ret = YES;
    if ([self.webViewProxyDelegate respondsToSelector:@selector(webView:shouldStartLoadWithRequest:navigationType:)]) {
        ret = [self.webViewProxyDelegate webView:webView shouldStartLoadWithRequest:request navigationType:navigationType];
    }
    
    if ([self.webBridgeDelegate respondsToSelector:@selector(webView:shouldStartLoadWithRequest:navigationType:)]) {
        ret = [self.webBridgeDelegate webView:webView shouldStartLoadWithRequest:request navigationType:navigationType];
    }
    
    BOOL isFragmentJump = NO;
    if (request.URL.fragment) {
        NSString *nonFragmentURL = [request.URL.absoluteString stringByReplacingOccurrencesOfString:[@"#" stringByAppendingString:request.URL.fragment] withString:@""];
        isFragmentJump = [nonFragmentURL isEqualToString:webView.request.URL.absoluteString];
    }
    
    BOOL isTopLevelNavigation = [request.mainDocumentURL isEqual:request.URL];
    
    BOOL isHTTP = [request.URL.scheme isEqualToString:@"http"] || [request.URL.scheme isEqualToString:@"https"];
    if (ret && !isFragmentJump && isHTTP && isTopLevelNavigation) {
        [self setValue:request.URL forKey:@"_currentURL"];
        [self reset];
    }
    return ret;
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    if ([self.webViewProxyDelegate respondsToSelector:@selector(webViewDidStartLoad:)]) {
        [self.webViewProxyDelegate webViewDidStartLoad:webView];
    }
    
    if ([self.webBridgeDelegate respondsToSelector:@selector(webViewDidStartLoad:)]) {
        [self.webBridgeDelegate webViewDidStartLoad:webView];
    }
    
    
    NSUInteger _loadingCount = [[self valueForKey:@"_loadingCount"] integerValue];
    NSUInteger _maxLoadCount = [[self valueForKey:@"_maxLoadCount"] integerValue];
    
    _loadingCount++;
    _maxLoadCount = fmax(_maxLoadCount, _loadingCount);
    [self setValue:@(_loadingCount) forKey:@"_loadingCount"];
    [self setValue:@(_maxLoadCount) forKey:@"_maxLoadCount"];
    
    [self performSelector:@selector(startProgress)];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    
    if ([self.webViewProxyDelegate respondsToSelector:@selector(webViewDidFinishLoad:)]) {
        [self.webViewProxyDelegate webViewDidFinishLoad:webView];
    }
    
    if (self.webBridgeDelegate && [self.webBridgeDelegate respondsToSelector:@selector(webViewDidFinishLoad:)]) {
        id webViewDelegate = [(NSObject *)self.webBridgeDelegate valueForKey:@"_webViewDelegate"];
        
        if ( !webViewDelegate || !self.webViewProxyDelegate || webViewDelegate != self.webViewProxyDelegate ) {
            [self.webBridgeDelegate webViewDidFinishLoad:webView];
        }
    }
    
    NSUInteger _loadingCount = [[self valueForKey:@"_loadingCount"] integerValue];
    _loadingCount--;
    [self setValue:@(_loadingCount) forKey:@"_loadingCount"];
    
    [self performSelector:@selector(incrementProgress)];
    
    NSString *readyState = [webView stringByEvaluatingJavaScriptFromString:@"document.readyState"];
    
    BOOL interactive = [readyState isEqualToString:@"interactive"];
    if (interactive) {
        //        BOOL _interactive = [[self valueForKey:@"_interactive"] boolValue];
        [self setValue:@(YES) forKey:@"_interactive"];
        //        _interactive = YES;
        
        NSString *completeRPCURL = @"webviewprogressproxy:///complete";
        NSString *waitForCompleteJS = [NSString stringWithFormat:@"window.addEventListener('load',function() { var iframe = document.createElement('iframe'); iframe.style.display = 'none'; iframe.src = '%@'; document.body.appendChild(iframe);  }, false);", completeRPCURL];
        [webView stringByEvaluatingJavaScriptFromString:waitForCompleteJS];
    }
    NSURL *_currentURL = [self valueForKey:@"_currentURL"];
    BOOL isNotRedirect = _currentURL && [_currentURL isEqual:webView.request.mainDocumentURL];
    BOOL complete = [readyState isEqualToString:@"complete"];
    if (complete && isNotRedirect) {
        [self performSelector:@selector(completeProgress)];
    }
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    if ([self.webViewProxyDelegate respondsToSelector:@selector(webView:didFailLoadWithError:)]) {
        [self.webViewProxyDelegate webView:webView didFailLoadWithError:error];
    }
    
    if ([self.webBridgeDelegate respondsToSelector:@selector(webView:didFailLoadWithError:)]) {
        [self.webBridgeDelegate webView:webView didFailLoadWithError:error];
    }
    
    NSUInteger _loadingCount = [[self valueForKey:@"_loadingCount"] integerValue];
    _loadingCount--;
    [self setValue:@(_loadingCount) forKey:@"_loadingCount"];
    [self performSelector:@selector(incrementProgress)];
    
    NSString *readyState = [webView stringByEvaluatingJavaScriptFromString:@"document.readyState"];
    
    BOOL interactive = [readyState isEqualToString:@"interactive"];
    if (interactive) {
        //        _interactive = YES;
        [self setValue:@(YES) forKey:@"_interactive"];
        NSString *completeRPCURL = @"webviewprogressproxy:///complete";
        NSString *waitForCompleteJS = [NSString stringWithFormat:@"window.addEventListener('load',function() { var iframe = document.createElement('iframe'); iframe.style.display = 'none'; iframe.src = '%@'; document.body.appendChild(iframe);  }, false);", completeRPCURL];
        [webView stringByEvaluatingJavaScriptFromString:waitForCompleteJS];
    }
    NSURL *_currentURL = [self valueForKey:@"_currentURL"];
    BOOL isNotRedirect = _currentURL && [_currentURL isEqual:webView.request.mainDocumentURL];
    BOOL complete = [readyState isEqualToString:@"complete"];
    if (complete && isNotRedirect) {
        [self performSelector:@selector(completeProgress)];
    }
}





@end
