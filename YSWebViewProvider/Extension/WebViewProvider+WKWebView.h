//
//  WebViewProvider+WKWebView.h
//  PlayApp
//
//  Created by yans on 2017/1/19.
//  Copyright © 2017年 hzty. All rights reserved.
//

#import "WebViewProvider.h"

@interface WebViewProvider (WKWebView)<WKScriptMessageHandler>

#pragma mark - WKWebView Delegate Methods

@property (nonatomic, copy) void(^decidePolicyForNavigationAction)(WKWebView *,WKNavigationAction *,WKNavigationActionPolicyBlock);
@property (nonatomic, copy) void(^didStartProvisionalNavigation)(WKWebView *,WKNavigation *);
@property (nonatomic, copy) void(^didFailProvisionalNavigation)(WKWebView *,WKNavigation *,NSError *);
@property (nonatomic, copy) void(^didCommitNavigation)(WKWebView *,WKNavigation *);
@property (nonatomic, copy) void(^didFailNavigation)(WKWebView *,WKNavigation *,NSError *);
@property (nonatomic, copy) void(^didFinishNavigation)(WKWebView *,WKNavigation *);
@property (nonatomic, copy) void(^decidePolicyForNavigationResponse)(WKWebView *,WKNavigationResponse *,WKNavigationResponsePolicyBlock);
@property (nonatomic, copy) void(^didReceiveScriptMessage)(WKUserContentController *,WKScriptMessage *);

#pragma mark - 

- (id)createWKWebViewWithFrame:(CGRect)frame;

- (id)createWKWebViewWithFrame:(CGRect)frame config:(WKWebViewConfiguration *)config;

@end
