//
//  UIProgressView+WebKit.h
//  PlayApp
//
//  Created by yans on 2017/1/19.
//  Copyright © 2017年 hzty. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIProgressView (WebKit)
/// Hidden when progress approach 1.0 Default is NO.
@property(assign, nonatomic) BOOL hiddenWhenProgressApproachFullSize;
/// The web view controller.
@end
