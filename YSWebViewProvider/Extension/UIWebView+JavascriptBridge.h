//
//  UIWebView+JavascriptBridge.h
//  PlayApp
//
//  Created by YANS on 2016/12/22.
//  Copyright © 2016年 hzty. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebViewJavascriptBridgeDelegate.h"

@interface UIWebView (JavascriptBridge)<WebViewJavascriptBridgeDelegate>

@end
