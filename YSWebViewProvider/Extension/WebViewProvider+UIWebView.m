//
//  WebViewProvider+UIWebView.m
//  PlayApp
//
//  Created by yans on 2017/1/19.
//  Copyright © 2017年 hzty. All rights reserved.
//

#import "WebViewProvider+UIWebView.h"
#import <objc/runtime.h>

@implementation WebViewProvider (UIWebView)

#pragma mark - shouldStartLoadWithRequest

- (BOOL (^)(UIWebView *, NSURLRequest *, UIWebViewNavigationType))shouldStartLoadWithRequest
{
    return objc_getAssociatedObject(self, _cmd);
}

- (void)setShouldStartLoadWithRequest:(BOOL (^)(UIWebView *, NSURLRequest *, UIWebViewNavigationType))shouldStartLoadWithRequest
{
    objc_setAssociatedObject(self, @selector(shouldStartLoadWithRequest), shouldStartLoadWithRequest, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

#pragma mark - webViewDidStartLoad

- (void (^)(UIWebView *))webViewDidStartLoad
{
    return objc_getAssociatedObject(self, _cmd);
}

- (void)setWebViewDidStartLoad:(void (^)(UIWebView *))webViewDidStartLoad
{
    objc_setAssociatedObject(self, @selector(webViewDidStartLoad), webViewDidStartLoad, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

#pragma mark - didFailLoadWithError

- (void (^)(UIWebView *, NSError *))didFailLoadWithError
{
    return objc_getAssociatedObject(self, _cmd);
}

- (void)setDidFailLoadWithError:(void (^)(UIWebView *, NSError *))didFailLoadWithError
{
    objc_setAssociatedObject(self, @selector(didFailLoadWithError), didFailLoadWithError, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

#pragma mark - webViewDidFinishLoad

- (void (^)(UIWebView *))webViewDidFinishLoad
{
    return objc_getAssociatedObject(self, _cmd);
}

- (void)setWebViewDidFinishLoad:(void (^)(UIWebView *))webViewDidFinishLoad
{
    objc_setAssociatedObject(self, @selector(webViewDidFinishLoad), webViewDidFinishLoad, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

#pragma mark -

- (id)createUIWebViewWithFrame:(CGRect)frame
{
    UIWebView *webView = [[UIWebView alloc] initWithFrame: frame];
    webView.backgroundColor = [UIColor clearColor];
    webView.scalesPageToFit = YES;
    if (WebViewProviderUseAutoLayout) {
        webView.translatesAutoresizingMaskIntoConstraints = NO;
    } else {
        webView.scrollView.contentInset = UIEdgeInsetsZero;
    }

//#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 110000
    if (@available(iOS 11.0, *)) {
        webView.scrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }
//#endif
    
    return webView;
}

#pragma mark - UIWebView Delegate Methods

/*
 * Called on iOS devices that do not have WKWebView when the UIWebView requests to start loading a URL request.
 * Note that it just calls shouldStartDecidePolicy, which is a shared delegate method.
 * Returning YES here would allow the request to complete, returning NO would stop it.
 */
- (BOOL) webView: (UIWebView *) webView shouldStartLoadWithRequest: (NSURLRequest *) request navigationType: (UIWebViewNavigationType) navigationType
{
    // URL actions
    if ([request.URL.absoluteString isEqualToString:kWP404NotFoundURLKey] || [request.URL.absoluteString isEqualToString:kWPNetworkErrorURLKey]) {
        [self loadURL:self.URL]; return NO;
    }
    // Resolve URL. Fixs the issue: https://github.com/devedbox/AXWebViewController/issues/7
    NSURLComponents *components = [[NSURLComponents alloc] initWithString:request.URL.absoluteString];
    NSURL *url = components.URL;
    NSString *ursString = url.absoluteString;

    // For appstore.
    if ([[NSPredicate predicateWithFormat:@"SELF BEGINSWITH[cd] 'https://itunes.apple.com/' OR SELF BEGINSWITH[cd] 'mailto:' OR SELF BEGINSWITH[cd] 'tel:' OR SELF BEGINSWITH[cd] 'telprompt:'"] evaluateWithObject:ursString]) {
        if ([[NSPredicate predicateWithFormat:@"SELF BEGINSWITH[cd] 'https://itunes.apple.com/'"] evaluateWithObject:ursString] && !self.reviewsAppInAppStore) {
            BOOL ret = [self findProductInfoWithUrl:ursString];
            if (ret) {
                return NO;
            }
        }
        
        [self.class applicationOpenURL:url completionHandler:nil];
//        return NO;
        return YES;
    } else if (![[NSPredicate predicateWithFormat:@"SELF MATCHES[cd] 'https' OR SELF MATCHES[cd] 'http' OR SELF MATCHES[cd] 'file' OR SELF MATCHES[cd] 'about'"] evaluateWithObject:components.scheme]) {// For any other schema.
        [self.class applicationOpenURL:url completionHandler:nil];
//        return NO;
        return YES;//否则自定义协议无法调起
    }
    switch (navigationType) {
        case UIWebViewNavigationTypeLinkClicked: {
#if __IPHONE_OS_VERSION_MIN_REQUIRED < __IPHONE_8_0
            [self pushCurrentSnapshotViewWithRequest:request];
#endif
            break;
        }
        case UIWebViewNavigationTypeFormSubmitted: {
#if __IPHONE_OS_VERSION_MIN_REQUIRED < __IPHONE_8_0
            [self pushCurrentSnapshotViewWithRequest:request];
#endif
            break;
        }
        case UIWebViewNavigationTypeBackForward: {
            break;
        }
        case UIWebViewNavigationTypeReload: {
            break;
        }
        case UIWebViewNavigationTypeFormResubmitted: {
            break;
        }
        case UIWebViewNavigationTypeOther: {
#if __IPHONE_OS_VERSION_MIN_REQUIRED < __IPHONE_8_0
            [self pushCurrentSnapshotViewWithRequest:request];
#endif
            break;
        }
        default: {
            break;
        }
    }

    
    if (self.shouldStartLoadWithRequest) {
        return self.shouldStartLoadWithRequest(webView,request,navigationType);
    }
    
    return [self shouldStartDecidePolicy: request];
}

/*
 * Called on iOS devices that do not have WKWebView when the UIWebView starts loading a URL request.
 * Note that it just calls didStartNavigation, which is a shared delegate method.
 */
- (void) webViewDidStartLoad: (UIWebView *) webView
{
    if (self.webViewDidStartLoad) {
        self.webViewDidStartLoad(webView);
    }
    
    [self didStartNavigation:webView];
}

/*
 * Called on iOS devices that do not have WKWebView when a URL request load failed.
 * Note that it just calls failLoadOrNavigation, which is a shared delegate method.
 */
- (void) webView: (UIWebView *) webView didFailLoadWithError: (NSError *) error
{
    if (error.code == NSURLErrorCancelled) {
//        [webView reload];
        return;
    }

    if (self.didFailLoadWithError) {
        self.didFailLoadWithError(webView,error);
    }
    
    [self failLoadOrNavigation: [webView request] withError: error];
}

/*
 * Called on iOS devices that do not have WKWebView when the UIWebView finishes loading a URL request.
 * Note that it just calls finishLoadOrNavigation, which is a shared delegate method.
 */
- (void) webViewDidFinishLoad: (UIWebView *) webView
{
    if (self.webViewDidFinishLoad) {
        self.webViewDidFinishLoad(webView);
    }
    
    [self finishLoadOrNavigation: [webView request]];
}





@end
