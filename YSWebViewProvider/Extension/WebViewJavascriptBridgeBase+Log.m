//
//  WebViewJavascriptBridgeBase+log.m
//  YSWebViewProvider
//
//  Created by yans on 2018/5/9.
//

#import <WebViewJavascriptBridge/WebViewJavascriptBridge.h>
#import "WVSwzzlingHeader.h"
#import "WVConfigHeader.h"

@interface WebViewJavascriptBridgeBase (Log)

@end

@implementation WebViewJavascriptBridgeBase (Log)

+ (void)load
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        wv_swizzling_exchangeMethod(self, @selector(_log:json:), @selector(wv_log:json:));
    });
}

- (void)wv_log:(NSString *)action json:(id)json {
    if (action && [action isEqualToString:@"RCVD"]) {
        NSDictionary *dic = json;
        if (dic && [dic isKindOfClass:[NSDictionary class]]) {
            NSString *handlerName = dic[@"handlerName"];
            if (handlerName && [handlerName isEqualToString:kWVWindowError]) {
                return;
            }
        }
    }
    [self wv_log:action json:json];
}

@end
