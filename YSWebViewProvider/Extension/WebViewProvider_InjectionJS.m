//
//  WebViewProvider+InjectionJS.m
//  Student
//
//  Created by yans on 2017/4/26.
//  Copyright © 2017年 yans. All rights reserved.
//

#import "WebViewProvider_InjectionJS.h"
#import "WVConfigHeader.h"

NSString *adCleanJS()
{ // document.querySelector('#onMenuShareAppMessage').onclick = function () {}
#define ___js_func__(x) #x
    NSString *preprocessorJS = @___js_func__(
                                             
                                             try {
                                                 console.log('ad observer ... start');
                                                 /*屏蔽中移动*/
                                                 var mobserver = new MutationObserver(function(mutations) {
                                                     mutations.forEach(function(mutation) {
                                                         var toolbar = document.getElementById('tlbstoolbar');
                                                         if(toolbar) {
                                                             document.getElementById('tlbstoolbar').style.display = 'none';
                                                             document.getElementById('tlbstoolbar').innerHTML = ' ';
                                                             document.getElementById('1qa2ws').style.display = 'none';
                                                             document.getElementById('1qa2ws').innerHTML = ' ';
                                                             /*随后,你还可以停止观察*/
                                                             mobserver.disconnect();
                                                             mobserver = null;
                                                             console.log('ad tlbstoolbar clean success.....');
                                                         }
                                                     });
                                                 });
                                                 mobserver.observe(document.querySelector('body'), { attributes: true, childList: true, characterData: true });
                                                 
                                                 setTimeout(function(){
                                                     if (mobserver) {
                                                         mobserver.disconnect();
                                                         mobserver = null;
                                                     }
                                                 },1000 * 60 * 10);
                                                 
                                             } catch (e) {
                                                 console.log('ppp:' + e);
                                             }
                                             ;
                                             
                                             );
    
#undef ___js_func__
    return preprocessorJS;
}

NSString *runReadyJS()
{
#define ___js_func__(x) #x
    static NSString *preprocessorJS = @___js_func__(
                                                    
                                                    (function(){
        
                                                        try {
                                                            if(window && window.WVJSApi && window.WVJSApi.WVFuns){
                                                                window.WVJSApi.jsReady = true;
                                                                window.WVJSApi.runReady();
                                                            }
                                                        } catch (e) {
                                                            elog('runReady err :' + e);
                                                        };
        
                                                    })();
                                                    
                                                    );
#undef ___js_func__
    return preprocessorJS;
};

NSString *formatTimeJS()
{
    NSString *js = @"\
    WVJSApi.formatTime = function(date) {\
        var year = date.getFullYear();\
        var month = date.getMonth() + 1;\
        var day = date.getDate();\
        var hour = date.getHours();\
        var minute = date.getMinutes();\
        var second = date.getSeconds();\
        var mill = date.getMilliseconds();\
        return [year, month, day].map(WVJSApi.formatNumber2).join('-') + ' ' + [hour, minute, second].map(WVJSApi.formatNumber2).join(':') + '.' + WVJSApi.formatNumber3(mill);\
    };\
    WVJSApi.formatNumber2 = function(n) {\
        n = n.toString();\
        return n[1] ? n : '0' + n;\
    };\
    WVJSApi.formatNumber3 = function(n) {\
        n = WVJSApi.formatNumber2(n);\
        return n[2] ? n : '0' + n;\
    };";
    return js;
}

NSString *injectionJS()
{
#define ___js_func__(x) #x
    static NSString *preprocessorJS = @___js_func__(
                                                    
                                                    var WVJSApi = {};
                                                    WVJSApi.uniqueId = 1;
#ifdef DEBUG
                                                    WVJSApi.debug = true;
#else
                                                    WVJSApi.debug = false;
#endif
                                                    {%@};
                                                    
                                                    var kWVWindowError = '%@';
                                                    
                                                    function xlog(message, data) {
                                                        if(!WVJSApi.debug) return;
                                                        var da = ' ';
                                                        if (data) {
                                                            if (typeof data == 'object') {
                                                                da = JSON.stringify(data);
                                                            }else{
                                                                da = data;
                                                            }
                                                        }
                                                        
                                                        var n = WVJSApi.uniqueId++ ;
                                                        if (n == 1) {
                                                            var bb = '%@';
                                                            console.log(bb + 'c  Well Come! ✈️✈️✈️✈️🚀🚀🚀🚀 ' + bb + 'c', 'background-image:-webkit-gradient(linear, left top, right top, color-stop(0, #f22), color-stop(0.15, #f2f), color-stop(0.3, #22f), color-stop(0.45, #2ff), color-stop(0.6, #2f2),color-stop(0.75, #2f2), color-stop(0.9, #ff2), color-stop(1, #f22));color:transparent;-webkit-background-clip: text;font-size:2.5em;font-family:\'HelveticaNeue-Light\';text-align:center','background: url(http://assets.91118.com/img/klxt2017/header/h-Logo1.png) no-repeat 0 10px;padding-left:139px;padding-top:40px;');
                                                        }
                                                        
                                                        var c = WVJSApi.formatTime(new Date()) + ' [' + WVJSApi.formatNumber3(n) + '] ' + message + da;
                                                        console.log(c);
                                                    }
                                                    
                                                    function elog(message, data) {
                                                        if(!WVJSApi.debug) return;
                                                        var da = ' ';
                                                        if (data) {
                                                            if (typeof data == 'object') {
                                                                da = JSON.stringify(data);
                                                            }else{
                                                                da = data;
                                                            }
                                                        }
                                                        
                                                        var n = WVJSApi.uniqueId++ ;
                                                        var c = WVJSApi.formatTime(new Date()) + ' [' + WVJSApi.formatNumber3(n) + '] ' + message + da;
                                                        console.error(c);
                                                    }
                                                    ;
                                                    WVJSApi.xlog = xlog;
                                                    WVJSApi.elog = elog;
                                                    
                                                    WVJSApi.WVFuns = [];
                                                    WVJSApi.errors = [];
                                                    WVJSApi.jsReady = false;
                                                    WVJSApi.ready = function(callback) {
                                                        if (typeof callback === 'function') {
                                                            if (WVJSApi.jsReady) {
                                                                callback();
                                                            } else {
                                                                WVJSApi.WVFuns.push(callback);
                                                            }
                                                            return true;
                                                        } else {
                                                            xlog('WVJSApi.ready,非法参数:' + callback + ',用法 WVJSApi.ready(function(){/* start your code */})  ');
                                                            return false;
                                                        }
                                                    };
                                                    
                                                    WVJSApi.runError = function(e) {
                                                        if (WVJSApi.jsReady) {
                                                            try {
                                                                eval(kWVWindowError + '(e)');
                                                            } catch (e) {
                                                                elog('runError -> ' + e);
                                                            }
                                                        }else{
                                                            WVJSApi.errors.push(e);
                                                        }
                                                    };
                                                    
                                                    WVJSApi.runReady = function() {
                                                        if (WVJSApi.jsReady) {
                                                            var __func_;
                                                            while ((__func_ = WVJSApi.WVFuns.shift())) {
                                                                __func_();
                                                            }
                                                            xlog('run ready finish ...');
                                                            
                                                            var err;
                                                            while ((err = WVJSApi.errors.shift())) {
                                                                WVJSApi.runError(err);
                                                            }
                                                        }
                                                    };
                                                    
                                                    window.onerror = function(errorMessage, scriptURI, lineNumber) {
                                                        var err = {};
                                                        err.errorMessage = errorMessage;
                                                        err.scriptURI = scriptURI;
                                                        err.lineNumber = lineNumber;
                                                        
                                                        xlog('windowError -> ', err);
                                                        WVJSApi.runError(err);
                                                    };
    
                                                    function setupWebViewJavascriptBridge(callback) {
                                                        if (window.WebViewJavascriptBridge) {
                                                            return callback(WebViewJavascriptBridge);
                                                        }
                                                        if (window.WVJBCallbacks) {
                                                            return window.WVJBCallbacks.push(callback);
                                                        }
                                                        window.WVJBCallbacks = [callback];
                                                        var WVJBIframe = document.createElement('iframe');
                                                        WVJBIframe.style.display = 'none';
                                                        /*WVJBIframe.src='wvjbscheme://__BRIDGE_LOADED__';*/
                                                        WVJBIframe.src = '%@://%@';
                                                        document.documentElement.appendChild(WVJBIframe);
                                                        setTimeout(function() { document.documentElement.removeChild(WVJBIframe); }, 0);
                                                    }
                                                    ;
                                                    WVJSApi.setupWebViewJavascriptBridge = setupWebViewJavascriptBridge;
                                                    
                                                    function bridgeCallHandler(methodName, data, callback) {
                                                        var da = ' ';
                                                        if (data) {
                                                            if (typeof data == 'object') {
                                                                da = ' , params -> ' + JSON.stringify(data);
                                                            }else{
                                                                da = ' , params -> ' + data;
                                                            }
                                                        }
                                                        
                                                        if (methodName != kWVWindowError) {
                                                            xlog('callHandler\t-> '+methodName + da);
                                                        }
                                                        
                                                        setupWebViewJavascriptBridge(function(bridge) {
                                                            bridge.callHandler(methodName, data, function(response) {
                                                                xlog('respHandler -> '+methodName + ' , data -> ', response);
                                                                if(callback) {
                                                                    callback(response);
                                                                }else{
                                                                    if (data) {
                                                                        var ff = data['callback'];
                                                                        if (ff && (typeof ff == "function" )) {
                                                                            ff(response);
                                                                        }
                                                                    }
                                                                }
                                                            });
                                                        });
                                                    }
                                                    ;
                                                    WVJSApi.bridgeCallHandler = bridgeCallHandler;
                                                    
                                                    function generateJSShortMethod(fn) {
                                                        var js =
                                                        ' var '+fn+' = function '+fn+'(data, callback) {'+
                                                        '     var tmp = arguments.callee.toString();'+
                                                        '     var re = /function\\s*(\\w*)/i;'+
                                                        '     var matches = re.exec(tmp);'+
                                                        '     var methodName = matches[1];'+
                                                        '     WVJSApi.bridgeCallHandler(methodName, data, callback);'+
                                                        ' };';
                                                        
                                                        var gname = window.globalNameSpace;
                                                        if(gname) {
                                                            js += 'window.' +gname + '.' + fn + ' = ' + fn + ';';
                                                        }
                                                        js += 'WVJSApi.'+fn+' = '+fn+';';
                                                        return js;
                                                    }
                                                    WVJSApi.generateJSShortMethod = generateJSShortMethod;
                                                    
                                                    
                                                    
                                                    
                                                    function __checkPlacehodelMethod(name) {
                                                        if (!WVJSApi.__before_call_list) {
                                                            WVJSApi.__before_call_list = [];
                                                        }
                                                        /*定义一个变量 防止多次 重复调用*/
                                                        if (WVJSApi.__before_call_list.length > 0) {
                                                            for (var i = 0; i < WVJSApi.__before_call_list.length; i++) {
                                                                if (name == WVJSApi.__before_call_list[i]) {
                                                                    return false;
                                                                }
                                                            }
                                                        }
                                                        WVJSApi.__before_call_list.push(name);
                                                        return true;
                                                    }
                                                    
                                                    function __removePlacehodelMethod(name) {
                                                        var index = -1;
                                                        for (var i = 0; i < WVJSApi.__before_call_list.length; i++) {
                                                            if (WVJSApi.__before_call_list[i] == name) {
                                                                index = i;
                                                            };
                                                        }
                                                        if (index > -1) {
                                                            WVJSApi.__before_call_list.splice(index, 1);
                                                        }
                                                    }
                                                    
                                                    WVJSApi.__prepareRegistrFunc = function(origMethod, placehodelMethod, namespace) {
                                                        if (!__checkPlacehodelMethod(placehodelMethod)) {
                                                            return false;
                                                        }
                                                        
                                                        var timerName = '__before_call_timer_' + origMethod;
                                                        var callTargetName = '__before_call_target_' + origMethod;
                                                        var fullMethodName = 'window.' + origMethod + ' = ';
                                                        var fullNameSpace = ' ';
                                                        if (namespace) {
                                                            fullNameSpace += 'if (window.' + namespace +'){}else{ window.' + namespace + ' = {}; } ';
                                                            fullNameSpace += 'window.' + namespace + '.' + origMethod + ' = ';
                                                        }
                                                        
                                                        xlog('proxy handler -> ' + origMethod + ' -> ' + placehodelMethod);
                                                        var js =
                                                        'function ' + callTargetName + '(data,cb) {' +
                                                        '  try {' +
                                                        '    if (' + placehodelMethod + ' && typeof(' + placehodelMethod + ') == \'function\') {' +
                                                        '      ' + placehodelMethod + '(data,cb);' +
                                                        '      clearInterval(' + timerName + ');' +
                                                        '      ' + timerName + ' = null;' +
                                                        '      xlog(\'proxy handler -> ' + placehodelMethod + ' , found , run finish... \');' +
                                                        '      /*重新定向回原始方法上*/' +
                                                        fullMethodName + placehodelMethod + ';' +
                                                        fullNameSpace + placehodelMethod + ';' +
                                                        'WVJSApi.' + origMethod + ' = ' + placehodelMethod + ';' +
                                                        '      __removePlacehodelMethod(' + placehodelMethod + ');' +
                                                        '    } else {' +
                                                        '         xlog(\'proxy handler -> ' + placehodelMethod + ' , not found\');' +
                                                        '    }' +
                                                        '  } catch (e) {' +
                                                        '         elog(\'proxy handler -> ' + callTargetName + ' , error: \' + e);' +
                                                        '  }' +
                                                        '}' +
                                                        'var ' + timerName + ' = null; ' +
                                                        'function ' + origMethod + '(data,cb) {' +
                                                        '  if (!' + timerName + ') {' +
                                                        /*'  ' + timerName + ' = setInterval(function() {' + ' ' + callTargetName + '(data,cb);' + ' }, 200);' +   */
                                                        '    ' + timerName + ' = WVJSApi.ready(function() {' + ' ' + callTargetName + '(data,cb);' + ' });' +
                                                        '  }' +
                                                        '}' +
                                                        fullMethodName + origMethod + ';' +
                                                        fullNameSpace + origMethod + ';' + 'WVJSApi.' + origMethod + ' = ' + origMethod + ';';
                                                        
                                                        xlog('proxy handler detail : \n' + js);
                                                        eval(js);
                                                    };
                                                    
                                                    window.WVJSApi = WVJSApi;
                                                    window.log = xlog;

                                                    xlog('api init ... ');

                                                    );
    preprocessorJS = [NSString stringWithFormat:preprocessorJS,formatTimeJS(),
                      kWVWindowError,
                      @"%",
                      kNewProtocolScheme,kBridgeLoaded
                      ];

#undef ___js_func__
    return preprocessorJS;
};

NSString *changeBridgeName(NSString *newName)
{
#define ___js_func__(x) #x
    NSString *preprocessorJS = @___js_func__(
                                             // begin js ..
                                             
                                             %@ = bridge;
                                             
                                             // end js ..
                                             );
    preprocessorJS = [NSString stringWithFormat:preprocessorJS,newName];
    
#undef ___js_func__
    return preprocessorJS;
};


/*
 
 // 2.3 监听“分享到QQ”按钮点击、自定义分享内容及分享结果接口
 document.querySelector('#onMenuShareQQ').onclick = function () {
 wx.onMenuShareQQ({
 title: '互联网之子',
 desc: '在长大的过程中，我才慢慢发现，我身边的所有事，别人跟我说的所有事，那些所谓本来如此，注定如此的事，它们其实没有非得如此，事情是可以改变的。更重要的是，有些事既然错了，那就该做出改变。',
 link: 'http://movie.douban.com/subject/25785114/',
 imgUrl: 'http://img3.douban.com/view/movie_poster_cover/spst/public/p2166127561.jpg',
 trigger: function (res) {
 alert('用户点击分享到QQ');
 },
 complete: function (res) {
 alert(JSON.stringify(res));
 },
 success: function (res) {
 alert('已分享');
 },
 cancel: function (res) {
 alert('已取消');
 },
 fail: function (res) {
 alert(JSON.stringify(res));
 }
 });
 alert('已注册获取“分享到 QQ”状态事件');
 };
 
 */


NSString *generateJSMethod(NSString *method)
{
    if (!method) {
        return @"";
    }
    
#define ___js_func__(x) #x
    NSString *preprocessorJS = @___js_func__(
                                             // begin js ..
                                             //-> method
                                             
                                             {
                                                 var _tjs = ' ';
                                                 var _tname = '%@';
                                                 try {
                                                     _tjs = WVJSApi.generateJSShortMethod(_tname);
                                                     eval(_tjs);
                                                     xlog('short handler -> '+_tname + '\t ok');
                                                 } catch (e) {
                                                     xlog('short handler -> '+_tname + ' , \t err -> '+e + '\n '+_tjs);
                                                 }
                                                 finally {
                                                     _tjs = ' ';
                                                 }
                                             };

                                             // end js ..
                                             );
    preprocessorJS = [NSString stringWithFormat:preprocessorJS, method];
    
#undef ___js_func__
    return preprocessorJS;
};

NSString *generateGlobalNameSpace(NSString *nameSpace)
{
    if (!nameSpace) {
        return @"";
    }
    
#define ___js_func__(x) #x
    NSString *preprocessorJS = @___js_func__(
                                             // begin js ..
                                             
                                             if(!window.globalNameSpace)
                                             {
                                                 window.globalNameSpace = '%@';
                                             }
                                             ;
                                             
                                             if(!window.%@)
                                             {
                                                 //-> namespace -> namespace
                                                 window.%@ = {};
                                             }
                                             ;
                                             
                                             // end js ..
                                             );
    preprocessorJS = [NSString stringWithFormat:preprocessorJS, nameSpace,
                      nameSpace,
                      nameSpace
                      ];
    
#undef ___js_func__
    return preprocessorJS;
};


NSString *generateJSMethodWithNP(NSString *method,NSString *nameSpace)
{
    if (!nameSpace) {
        return generateJSMethod(method);
    }
    
#define ___js_func__(x) #x
    NSString *preprocessorJS = @___js_func__(
                                             // begin js ..
                                             
                                             %@
                                             ;
                                             //-> namespace
                                             if(!window.%@)
                                             {
                                                 //-> namespace -> namespace
                                                 window.%@ = {};
                                             }
                                             //-> namespace -> method -> method
                                             window.%@.%@ = %@;
                                             
                                             if(WVJSApi){
                                                 WVJSApi.%@ = %@;
                                             }
                                             
                                             // end js ..
                                             );
    preprocessorJS = [NSString stringWithFormat:preprocessorJS, generateJSMethod(method),
                      nameSpace,
                      nameSpace,
                      nameSpace,method,method,
                      method,method
                      ];
    
#undef ___js_func__
    return preprocessorJS;
};

//页面上注册指定的 handle
NSString *generateRegisterHandlerJSMethod(NSString *method)
{
    if (!method) {
        return @"";
    }
    
#define ___js_func__(x) #x
    NSString *preprocessorJS = @___js_func__(
                                             // begin js ..
                                             //-> method
                                             
                                             try{
                                                 if(%@ && typeof(%@) == 'function'){
                                                     WebViewJavascriptBridge.registerHandler('%@', function(data, responseCallback) {
                                                         var responseData = %@(data);
                                                         responseCallback(responseData);
                                                     })
                                                 }else{
                                                     xlog('不存在的函数：%@');
                                                 }
                                             }catch(e){
                                                 elog(e);
                                             };
                                             
                                             // end js ..
                                             );
    preprocessorJS = [NSString stringWithFormat:preprocessorJS,method,method,method,method,method];
    
#undef ___js_func__
    return preprocessorJS;
};

/**
 
 __prepareRegistrFunc(origMethod, placehodelMethod, namespace)
 __prepareRegistrFunc('tttb', 'tttb_c')

 function tttb_c(data)
 {
    //todo..
 }

 @param origMethod 对外暴露的方法名称
 @param placehodelMethod 被转换后的方法名称
 @param nameSpace 命名空间
 
 */
NSString *prepareRegistrMethodJS(NSString *origMethod,NSString *placehodelMethod,NSString *nameSpace)
{
#define ___js_func__(x) #x
     NSString *preprocessorJS = @___js_func__(
    
                                              WVJSApi.__prepareRegistrFunc('%@','%@','%@');
        
                                             );
#undef ___js_func__
    preprocessorJS = [NSString stringWithFormat:preprocessorJS,
                      origMethod,placehodelMethod,nameSpace?nameSpace:@""];
    
    return preprocessorJS;
};

