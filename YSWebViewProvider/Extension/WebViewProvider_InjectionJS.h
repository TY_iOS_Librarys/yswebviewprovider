//
//  WebViewProvider+InjectionJS.h
//  Student
//
//  Created by yans on 2017/4/26.
//  Copyright © 2017年 yans. All rights reserved.
//

NSString *adCleanJS(void);
NSString *injectionJS(void);
NSString *generateGlobalNameSpace(NSString *nameSpace);
NSString *generateJSMethod(NSString *method);
NSString *generateJSMethodWithNP(NSString *method,NSString *nameSpace);
NSString *generateRegisterHandlerJSMethod(NSString *method);

NSString *runReadyJS(void);
NSString *prepareRegistrMethodJS(NSString *origMethod,NSString *placehodelMethod,NSString *nameSpace);
