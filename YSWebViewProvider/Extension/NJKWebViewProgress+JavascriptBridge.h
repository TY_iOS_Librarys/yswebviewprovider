//
//  NJKWebViewProgress+JavascriptBridge.h
//  PlayApp
//
//  Created by yans on 2017/1/19.
//  Copyright © 2017年 hzty. All rights reserved.
//

#import <NJKWebViewProgress/NJKWebViewProgress.h>

@interface NJKWebViewProgress (JavascriptBridge)

@property (nonatomic, njk_weak) id<UIWebViewDelegate>webBridgeDelegate;

@end
