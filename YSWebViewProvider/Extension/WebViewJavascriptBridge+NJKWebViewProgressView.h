//
//  WebViewJavascriptBridge+NJKWebViewProgressView.h
//  PlayApp
//
//  Created by yans on 2017/1/19.
//  Copyright © 2017年 hzty. All rights reserved.
//

#import <WebViewJavascriptBridge/WebViewJavascriptBridge.h>

@interface WebViewJavascriptBridge (NJKWebViewProgressView)

/** Andrew add method to compatible with NJK */

+ (instancetype)bridgeUserNJKForWebView:(id)webView;

+ (instancetype)bridgeUserNJKForWebView:(id)webView
                            handlerName:(NSString*)handlerName
                                handler:(WVJBHandler)messageHandler;

@end
