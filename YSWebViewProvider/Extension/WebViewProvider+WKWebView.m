//
//  WebViewProvider+WKWebView.m
//  PlayApp
//
//  Created by yans on 2017/1/19.
//  Copyright © 2017年 hzty. All rights reserved.
//

#import "WebViewProvider+WKWebView.h"
#import <objc/runtime.h>
#import "WKCookieSyncManager.h"

@implementation WebViewProvider (WKWebView)

#pragma mark - decidePolicyForNavigationAction

- (void (^)(WKWebView *, WKNavigationAction *, WKNavigationActionPolicyBlock))decidePolicyForNavigationAction
{
    return objc_getAssociatedObject(self, _cmd);
}

- (void)setDecidePolicyForNavigationAction:(void (^)(WKWebView *, WKNavigationAction *, WKNavigationActionPolicyBlock))decidePolicyForNavigationAction
{
    objc_setAssociatedObject(self, @selector(decidePolicyForNavigationAction), decidePolicyForNavigationAction, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

#pragma mark - didStartProvisionalNavigation

- (void (^)(WKWebView *, WKNavigation *))didStartProvisionalNavigation
{
    return objc_getAssociatedObject(self, _cmd);
}

- (void)setDidStartProvisionalNavigation:(void (^)(WKWebView *, WKNavigation *))didStartProvisionalNavigation
{
    objc_setAssociatedObject(self, @selector(didStartProvisionalNavigation), didStartProvisionalNavigation, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

#pragma mark - didFailProvisionalNavigation

- (void (^)(WKWebView *, WKNavigation *, NSError *))didFailProvisionalNavigation
{
    return objc_getAssociatedObject(self, _cmd);
}

- (void)setDidFailProvisionalNavigation:(void (^)(WKWebView *, WKNavigation *, NSError *))didFailProvisionalNavigation
{
    objc_setAssociatedObject(self, @selector(didFailProvisionalNavigation), didFailProvisionalNavigation, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

#pragma mark - didCommitNavigation

- (void (^)(WKWebView *, WKNavigation *))didCommitNavigation
{
    return objc_getAssociatedObject(self, _cmd);
}

- (void)setDidCommitNavigation:(void (^)(WKWebView *, WKNavigation *))didCommitNavigation
{
    objc_setAssociatedObject(self, @selector(didCommitNavigation), didCommitNavigation, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

#pragma mark - didFailNavigation

- (void (^)(WKWebView *, WKNavigation *, NSError *))didFailNavigation
{
    return objc_getAssociatedObject(self, _cmd);
}

- (void)setDidFailNavigation:(void (^)(WKWebView *, WKNavigation *, NSError *))didFailNavigation
{
    objc_setAssociatedObject(self, @selector(didFailNavigation), didFailNavigation, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

#pragma mark - didFinishNavigation

- (void (^)(WKWebView *, WKNavigation *))didFinishNavigation
{
    return objc_getAssociatedObject(self, _cmd);
}

- (void)setDidFinishNavigation:(void (^)(WKWebView *, WKNavigation *))didFinishNavigation
{
    objc_setAssociatedObject(self, @selector(didFinishNavigation), didFinishNavigation, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

#pragma mark - decidePolicyForNavigationResponse

- (void (^)(WKWebView *, WKNavigationResponse *, WKNavigationResponsePolicyBlock))decidePolicyForNavigationResponse
{
    return objc_getAssociatedObject(self, _cmd);
}

- (void)setDecidePolicyForNavigationResponse:(void (^)(WKWebView *, WKNavigationResponse *, WKNavigationResponsePolicyBlock))decidePolicyForNavigationResponse
{
    objc_setAssociatedObject(self, @selector(decidePolicyForNavigationResponse), decidePolicyForNavigationResponse, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

#pragma mark - didReceiveScriptMessage

- (void (^)(WKUserContentController *, WKScriptMessage *))didReceiveScriptMessage
{
    return objc_getAssociatedObject(self, _cmd);
}

- (void)setDidReceiveScriptMessage:(void (^)(WKUserContentController *, WKScriptMessage *))didReceiveScriptMessage
{
    objc_setAssociatedObject(self, @selector(didReceiveScriptMessage), didReceiveScriptMessage, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

#pragma mark -

- (id)createWKWebViewWithFrame:(CGRect)frame
{
    return [self createWKWebViewWithFrame:frame config:nil];
}

- (id)createWKWebViewWithFrame:(CGRect)frame config:(WKWebViewConfiguration *)config
{
    WKWebView *webView = [[WKWebView alloc] initWithFrame: frame configuration:config];
    webView.allowsBackForwardNavigationGestures = NO;
    webView.backgroundColor = [UIColor clearColor];
    webView.scrollView.backgroundColor = [UIColor clearColor];
    // Set auto layout enabled.
    
    if ([webView respondsToSelector:@selector(setAllowsLinkPreview:)]) {
        webView.allowsLinkPreview = NO;
    }
    
    if (WebViewProviderUseAutoLayout) {
        webView.translatesAutoresizingMaskIntoConstraints = NO;
    } else {
        webView.scrollView.contentInset = UIEdgeInsetsZero;
    }
    
//#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 110000
    if (@available(iOS 11.0, *)) {
        webView.scrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }
//#endif
    
    return webView;
}


#pragma mark - WKWebView Delegate Methods

/*
 * Called on iOS devices that have WKWebView when the web view wants to start navigation.
 * Note that it calls shouldStartDecidePolicy, which is a shared delegate method,
 * but it's essentially passing the result of that method into decisionHandler, which is a block.
 */
- (void) webView: (WKWebView *) webView decidePolicyForNavigationAction: (WKNavigationAction *) navigationAction decisionHandler: (void (^)(WKNavigationActionPolicy)) decisionHandler
{
    //wk注入 方式二：
    //        @weakify(self)
    //        [[self webView] evaluateJavaScript:injectionJS() completionHandler:^(id data, NSError *error) {
    //            @strongify(self)
    //            self -> _configInjectJs = YES;
    //        }];
    //    [self runInjectJsCode];
    
    // Disable all the '_blank' target in page's target.
    if (!navigationAction.targetFrame.isMainFrame) {
        [webView evaluateJavaScript:@"var a = document.getElementsByTagName('a');for(var i=0;i<a.length;i++){a[i].setAttribute('target','');}" completionHandler:nil];
    }
    // Resolve URL. Fixs the issue: https://github.com/devedbox/AXWebViewController/issues/7
    // !!!: Fixed url handleing of navigation request instead of main url.
    // NSURLComponents *components = [[NSURLComponents alloc] initWithString:webView.URL.absoluteString];
    NSURLComponents *components = [[NSURLComponents alloc] initWithString:navigationAction.request.URL.absoluteString];
    // For appstore and system defines. This action will jump to AppStore app or the system apps.
    NSURL *url = components.URL;
    NSString *ursString = url.absoluteString;

    if ([[NSPredicate predicateWithFormat:@"SELF BEGINSWITH[cd] 'https://itunes.apple.com/' OR SELF BEGINSWITH[cd] 'mailto:' OR SELF BEGINSWITH[cd] 'tel:' OR SELF BEGINSWITH[cd] 'telprompt:'"] evaluateWithObject:ursString]) {
        if ([[NSPredicate predicateWithFormat:@"SELF BEGINSWITH[cd] 'https://itunes.apple.com/'"] evaluateWithObject:ursString] && !self.reviewsAppInAppStore) {
            
            BOOL ret = [self findProductInfoWithUrl:ursString];
            if (ret) {
                decisionHandler(WKNavigationActionPolicyCancel);
                return;
            }
        }
        
        [self.class applicationOpenURL:url completionHandler:nil];
//        decisionHandler(WKNavigationActionPolicyCancel);
        decisionHandler(WKNavigationActionPolicyAllow);
        return;
    } else if (components.scheme && ![[NSPredicate predicateWithFormat:@"SELF MATCHES[cd] 'https' OR SELF MATCHES[cd] 'http' OR SELF MATCHES[cd] 'file' OR SELF MATCHES[cd] 'about'"] evaluateWithObject:components.scheme]) {// For any other schema but not `https`、`http` and `file`.
        [self.class applicationOpenURL:url completionHandler:nil];
//        decisionHandler(WKNavigationActionPolicyCancel);
        decisionHandler(WKNavigationActionPolicyAllow);
        return;
    }
    
    // URL actions for 404 and Errors:
    if ([[NSPredicate predicateWithFormat:@"SELF ENDSWITH[cd] %@ OR SELF ENDSWITH[cd] %@", kWP404NotFoundURLKey, kWPNetworkErrorURLKey] evaluateWithObject:ursString]) {
        // Reload the original URL.
        [self loadURL:self.URL];
    }
    
    if (self.decidePolicyForNavigationAction) {
        self.decidePolicyForNavigationAction(webView,navigationAction,decisionHandler);
    }
    
    decisionHandler([self shouldStartDecidePolicy: [navigationAction request]]);
}

- (void)webView:(WKWebView *)webView decidePolicyForNavigationResponse:(WKNavigationResponse *)navigationResponse decisionHandler:(void (^)(WKNavigationResponsePolicy))decisionHandler {
    
    if (self.decidePolicyForNavigationResponse) {
        self.decidePolicyForNavigationResponse(webView,navigationResponse,decisionHandler);
    }
    
    decisionHandler(WKNavigationResponsePolicyAllow);
}

/*
 * Called on iOS devices that have WKWebView when the web view starts loading a URL request.
 * Note that it just calls didStartNavigation, which is a shared delegate method.
 */
- (void) webView: (WKWebView *) webView didStartProvisionalNavigation: (WKNavigation *) navigation
{
    if (self.didStartProvisionalNavigation) {
        self.didStartProvisionalNavigation(webView,navigation);
    }
    [self didStartNavigation:webView];
}

/*
 * Called on iOS devices that have WKWebView when the web view fails to load a URL request.
 * Note that it just calls failLoadOrNavigation, which is a shared delegate method,
 * but it has to retrieve the active request from the web view as WKNavigation doesn't contain a reference to it.
 */
- (void) webView:(WKWebView *) webView didFailProvisionalNavigation: (WKNavigation *) navigation withError: (NSError *) error
{
    if (error.code == NSURLErrorCancelled) {
        // [webView reloadFromOrigin];
        return;
    }

    if (self.didFailProvisionalNavigation) {
        self.didFailProvisionalNavigation(webView,navigation,error);
    }
    UIView<FLWebViewProvider> *webView_ = webView;
    [self failLoadOrNavigation: [webView_ request] withError: error];
}

/*
 * Called on iOS devices that have WKWebView when the web view begins loading a URL request.
 * This could call some sort of shared delegate method, but is unused currently.
 */
- (void) webView: (WKWebView *) webView didCommitNavigation: (WKNavigation *) navigation
{
    // do nothing
    
    if (self.didCommitNavigation) {
        self.didCommitNavigation(webView,navigation);
    }
}

/*
 * Called on iOS devices that have WKWebView when the web view fails to load a URL request.
 * Note that it just calls failLoadOrNavigation, which is a shared delegate method.
 */
- (void) webView: (WKWebView *) webView didFailNavigation: (WKNavigation *) navigation withError: (NSError *) error
{
    if (error.code == NSURLErrorCancelled) {
//        [webView reloadFromOrigin];
        return;
    }

    if (self.didFailNavigation) {
        self.didFailNavigation(webView,navigation,error);
    }
    UIView<FLWebViewProvider> *webView_ = webView;
    [self failLoadOrNavigation: [webView_ request] withError: error];
}

/*
 * Called on iOS devices that have WKWebView when the web view finishes loading a URL request.
 * Note that it just calls finishLoadOrNavigation, which is a shared delegate method.
 */
- (void) webView: (WKWebView *) webView didFinishNavigation: (WKNavigation *) navigation
{
    [[WKCookieSyncManager sharedWKCookieSyncManager] setCookies:webView];

    if (self.didFinishNavigation) {
        self.didFinishNavigation(webView,navigation);
    }
    
    UIView<FLWebViewProvider> *webView_ = webView;
    [self finishLoadOrNavigation: [webView_ request]];
}

- (void)webView:(WKWebView *)webView didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition disposition, NSURLCredential *__credential))completionHandler {

    // !!!: Do add the security policy if using a custom credential.
    NSURLSessionAuthChallengeDisposition disposition = NSURLSessionAuthChallengePerformDefaultHandling;
    __block NSURLCredential *credential = nil;
    
    if (self.challengeHandler) {
        disposition = self.challengeHandler(webView, challenge, &credential);
    } else {
        if ([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust]) {
            if ([self.securityPolicy evaluateServerTrust:challenge.protectionSpace.serverTrust forDomain:challenge.protectionSpace.host]) {
                credential = [NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust];
                if (credential) {
                    disposition = NSURLSessionAuthChallengeUseCredential;
                } else {
                    disposition = NSURLSessionAuthChallengePerformDefaultHandling;
                }
            } else {
                disposition = NSURLSessionAuthChallengePerformDefaultHandling;
            }
        } else {
            disposition = NSURLSessionAuthChallengePerformDefaultHandling;
        }
    }
    
    if (completionHandler) {
        completionHandler(disposition, credential);
    }

}

- (void)webViewWebContentProcessDidTerminate:(WKWebView *)webView {
    // Get the host name.
    NSString *host = webView.URL.host;
    // Initialize alert view controller.
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:host?:@"来自网页的消息" message:@"网页进程终止" preferredStyle:UIAlertControllerStyleAlert];
    // Initialize cancel action.
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:NULL];
    // Initialize ok action.
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [alert dismissViewControllerAnimated:YES completion:NULL];
    }];
    // Add actions.
    [alert addAction:cancelAction];
    [alert addAction:okAction];
    
    [self runJavaScriptPanelWithMessage:nil alertController:alert];//2017.4.6

}

#pragma mark - WKUIDelegate
- (WKWebView *)webView:(WKWebView *)webView createWebViewWithConfiguration:(WKWebViewConfiguration *)configuration forNavigationAction:(WKNavigationAction *)navigationAction windowFeatures:(WKWindowFeatures *)windowFeatures {
    WKFrameInfo *frameInfo = navigationAction.targetFrame;
    if (![frameInfo isMainFrame]) {
        if (navigationAction.request) {
            [webView loadRequest:navigationAction.request];
        }
    }
    return nil;
}

- (void)webViewDidClose:(WKWebView *)webView {
    
}

- (void)webView:(WKWebView *)webView runJavaScriptAlertPanelWithMessage:(NSString *)message initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(void))completionHandler {
    // Get host name of url.
    NSString *host = webView.URL.host;
    // Init the alert view controller.
    NSString *title = host?:@"来自网页的消息";
    title = nil;
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle: UIAlertControllerStyleAlert];
    // Init the cancel action.
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        [alert dismissViewControllerAnimated:YES completion:NULL];
        completionHandler();
    }];
    // Init the ok action.
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [alert dismissViewControllerAnimated:YES completion:NULL];
        completionHandler();
    }];
    // Add actions.
    [alert addAction:cancelAction];
    [alert addAction:okAction];
    
    [self runJavaScriptPanelWithMessage:message alertController:alert];//2017.4.6
}

- (void)webView:(WKWebView *)webView runJavaScriptConfirmPanelWithMessage:(NSString *)message initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(BOOL result))completionHandler {
    // Get the host name.
    NSString *host = webView.URL.host;
    // Initialize alert view controller.
    NSString *title = host?:@"来自网页的消息";
    title = nil;

    UIAlertController* alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    // Initialize cancel action.
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        [alert dismissViewControllerAnimated:YES completion:NULL];
        completionHandler(NO);
    }];
    // Initialize ok action.
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [alert dismissViewControllerAnimated:YES completion:NULL];
        completionHandler(YES);
    }];
    // Add actions.
    [alert addAction:cancelAction];
    [alert addAction:okAction];
    
    [self runJavaScriptPanelWithMessage:message alertController:alert];//2017.4.6
  
}

- (void)webView:(WKWebView *)webView runJavaScriptTextInputPanelWithPrompt:(NSString *)prompt defaultText:(NSString *)defaultText initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(NSString * __result))completionHandler {
    // Get the host of url.
    NSString *host = webView.URL.host;

    // Initialize alert view controller.
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:prompt?:@"来自网页的消息" message:host preferredStyle:UIAlertControllerStyleAlert];
    // Add text field.
    [alert addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.placeholder = defaultText?:@"输入文字消息";
        textField.font = [UIFont systemFontOfSize:12];
    }];
    // Initialize cancel action.
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        [alert dismissViewControllerAnimated:YES completion:NULL];
        // Get inputed string.
        NSString *string = [alert.textFields firstObject].text;
        completionHandler(string?:defaultText);
    }];
    // Initialize ok action.
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [alert dismissViewControllerAnimated:YES completion:NULL];
        // Get inputed string.
        NSString *string = [alert.textFields firstObject].text;
        completionHandler(string?:defaultText);
    }];
    // Add actions.
    [alert addAction:cancelAction];
    [alert addAction:okAction];
    
    [self runJavaScriptPanelWithMessage:nil alertController:alert];//2017.4.6
}

#pragma mark - WKUIDelegate WKScriptMessageHandler
- (void)userContentController:(WKUserContentController *)userContentController didReceiveScriptMessage:(WKScriptMessage *)message
{
    if (self.didReceiveScriptMessage) {
        self.didReceiveScriptMessage(userContentController,message);
    }
    
}


@end
