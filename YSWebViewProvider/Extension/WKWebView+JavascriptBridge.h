//
//  WKWebView+JavascriptBridge.h
//  PlayApp
//
//  Created by YANS on 2016/12/22.
//  Copyright © 2016年 hzty. All rights reserved.
//

#import <WebKit/WebKit.h>
#import "WebViewJavascriptBridgeDelegate.h"

@interface WKWebView (JavascriptBridge)<WebViewJavascriptBridgeDelegate>

@end
