//
//  WebViewJavascriptBridge+NJKWebViewProgressView.m
//  PlayApp
//
//  Created by yans on 2017/1/19.
//  Copyright © 2017年 hzty. All rights reserved.
//

#import "WebViewJavascriptBridge+NJKWebViewProgressView.h"

@implementation WebViewJavascriptBridge (NJKWebViewProgressView)
/** Andrew add method to compatible with NJK */

+ (instancetype)bridgeUserNJKForWebView:(id)webView
{
    return [self bridgeUserNJKForWebView:webView handlerName:nil handler:nil];
}

+ (instancetype)bridgeUserNJKForWebView:(id)webView
                            handlerName:(NSString*)handlerName
                                handler:(WVJBHandler)messageHandler;
{
    WebViewJavascriptBridge *bridge = [[self alloc] init];
    [bridge _platformNJKSpecificSetup:webView handlerName:handlerName
                              handler:messageHandler resourceBundle:nil];
    return bridge;
}

- (void) _platformNJKSpecificSetup:(WVJB_WEBVIEW_TYPE*)webView
                       handlerName:(NSString*)handlerName
                           handler:(WVJBHandler)messageHandler
                    resourceBundle:(NSBundle*)bundle{
    
    [self setValue:webView forKey:@"_webView"];
    [self setValue:nil forKey:@"_webViewDelegate"];
    WebViewJavascriptBridgeBase *_base = [[WebViewJavascriptBridgeBase alloc] init];
    if (messageHandler) {
        _base.messageHandler = messageHandler;
    }
    if (handlerName && messageHandler) {
        _base.messageHandlers[handlerName] = [messageHandler copy];
    }
    _base.delegate = self;
    
    [self valueForKey:@"_base"];
}
@end
