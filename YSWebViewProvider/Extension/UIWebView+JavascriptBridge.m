//
//  UIWebView+JavascriptBridge.m
//  PlayApp
//
//  Created by YANS on 2016/12/22.
//  Copyright © 2016年 hzty. All rights reserved.
//

#import "UIWebView+JavascriptBridge.h"
#import "WebViewJavascriptBridge.h"
#import <objc/runtime.h>

@implementation UIWebView (JavascriptBridge)

/*
 * Getter for the active request. UIWebView has this, but WKWebView does not, so we add it here.
 */
- (WebViewJavascriptBridge *) javascriptBridge
{
    return objc_getAssociatedObject(self, _cmd);
}

/*
 * Setter for the active request.
 */
- (void) setJavascriptBridge:(WebViewJavascriptBridge *)javascriptBridge
{
    objc_setAssociatedObject(self, @selector(javascriptBridge), javascriptBridge, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}


- (void)bridgeForWebViewWithDelegate:(id)delegate
{
    [WebViewJavascriptBridge enableLogging];
    
    [self setJavaScriptBridgeWebViewDelegate:delegate];

}

- (void)setJavaScriptBridgeWebViewDelegate:(id)delegate
{
    if (!self.javascriptBridge) {
        self.javascriptBridge = [WebViewJavascriptBridge bridgeForWebView:self];
    }
    [self.javascriptBridge setWebViewDelegate:delegate];
}

//- (void)setJavaScriptBridgeUserNJKWebViewDelegate:(id)delegate
//{
//    if (!self.javascriptBridge) {
//        self.javascriptBridge = [WebViewJavascriptBridge bridgeUserNJKForWebView:self];
//    }
//    [self.javascriptBridge setWebViewDelegate:delegate];
//}

- (void)registerHandler:(NSString *)handlerName handler:(WVJBHandler)handler
{
    [self.javascriptBridge registerHandler:handlerName handler:handler];
}

- (void)callHandler:(NSString *)handlerName data:(id)data responseCallback:(WVJBResponseCallback)responseCallback
{
    [self.javascriptBridge callHandler:handlerName data:data responseCallback:responseCallback];
}


- (NSString *)title
{
    return [self stringByEvaluatingJavaScriptFromString:@"document.title"];
}

- (NSString *)host
{
    return self.request.URL.host;
}


@end
