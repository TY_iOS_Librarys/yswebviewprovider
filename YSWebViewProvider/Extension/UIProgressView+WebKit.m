//
//  UIProgressView+WebKit.m
//  PlayApp
//
//  Created by yans on 2017/1/19.
//  Copyright © 2017年 hzty. All rights reserved.
//

#import "UIProgressView+WebKit.h"
#import <objc/runtime.h>


@implementation UIProgressView (WebKit)

//@dynamic hiddenWhenProgressApproachFullSize;

+ (void)load {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        // Inject "-popViewControllerAnimated:"
        Method originalMethod = class_getInstanceMethod(self, @selector(setProgress:));
        Method swizzledMethod = class_getInstanceMethod(self, @selector(ys_setProgress:));
        method_exchangeImplementations(originalMethod, swizzledMethod);
        
        originalMethod = class_getInstanceMethod(self, @selector(setProgress:animated:));
        swizzledMethod = class_getInstanceMethod(self, @selector(ys_setProgress:animated:));
        method_exchangeImplementations(originalMethod, swizzledMethod);
    });
}

- (void)ys_setProgress:(float)progress {
    [self ys_setProgress:progress];
    
    [self checkHiddenWhenProgressApproachFullSize];
}

- (void)ys_setProgress:(float)progress animated:(BOOL)animated {
    [self ys_setProgress:progress animated:animated];
    
    [self checkHiddenWhenProgressApproachFullSize];
}

- (void)checkHiddenWhenProgressApproachFullSize {
    if (!self.hiddenWhenProgressApproachFullSize) {
        return;
    }
    
    float progress = self.progress;
    if (progress < 1) {
        if (self.hidden) {
            self.hidden = NO;
        }
    } else if (progress >= 1) {
        [UIView animateWithDuration:0.35 delay:0.15 options:7 animations:^{
            self.alpha = 0.0;
        } completion:^(BOOL finished) {
            if (finished) {
                self.hidden = YES;
                self.progress = 0.0;
                self.alpha = 1.0;
                // Update the navigation itmes if the delegate is not being triggered.
            }
        }];
    }
}

- (BOOL)hiddenWhenProgressApproachFullSize {
    return [objc_getAssociatedObject(self, _cmd) boolValue];
}

- (void)setHiddenWhenProgressApproachFullSize:(BOOL)hiddenWhenProgressApproachFullSize {
    objc_setAssociatedObject(self, @selector(hiddenWhenProgressApproachFullSize), @(hiddenWhenProgressApproachFullSize), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

@end
