//
//  WebViewHelper.h
//  WebViewHelper
//
//  Created by yans on 2017/1/13.
//  Copyright © 2017年 hzty. All rights reserved.
//

#import "FLWebViewProvider.h"
#import "UIWebView+FLUIWebView.h"
#import "WKWebView+FLWKWebView.h"

#import "WebViewProvider.h"
#import "WKCookieSyncManager.h"
#import "WeakScriptMessageDelegate.h"

#define __IOS11_OR_LATER        ( [[[UIDevice currentDevice] systemVersion] compare:@"11.0" options:NSNumericSearch] != NSOrderedAscending )
#define __IOS10_OR_LATER        ( [[[UIDevice currentDevice] systemVersion] compare:@"10.0" options:NSNumericSearch] != NSOrderedAscending )
#define __IOS9_OR_LATER        ( [[[UIDevice currentDevice] systemVersion] compare:@"9.0" options:NSNumericSearch] != NSOrderedAscending )
#define __IOS8_OR_LATER        ( [[[UIDevice currentDevice] systemVersion] compare:@"8.0" options:NSNumericSearch] != NSOrderedAscending )

#define __IOS8_OR_EARLIER        ( !__IOS9_OR_LATER )
#define __IOS7_OR_EARLIER        ( !__IOS8_OR_LATER )

