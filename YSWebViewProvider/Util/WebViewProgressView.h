//
//  WebViewProgressView.h
//  PlayApp
//
//  Created by yans on 2017/1/19.
//  Copyright © 2017年 hzty. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FLWebViewProvider.h"
#import "WebViewSharedDelegate.h"
#import "WebViewJavascriptBridgeDelegate.h"

@interface WebViewProgressView : UIView

@property (nonatomic, strong) UIColor *progressTintColor;
@property (nonatomic, assign) float progress;
@property (nonatomic, copy) void (^progressUpdateBlock)(float progress,BOOL animated);

- (instancetype)initWithFrame:(CGRect)frame
                      webView:(UIView<FLWebViewProvider,WebViewJavascriptBridgeDelegate,WebViewSharedDelegate> *)webView;

- (instancetype)initWithFrame:(CGRect)frame
                      webView:(UIView<FLWebViewProvider,
                               WebViewJavascriptBridgeDelegate,WebViewSharedDelegate> *)webView
               targetDelegate:(id<UIWebViewDelegate>)targetDelegate;

- (void)setProgress:(float)progress animated:(BOOL)animated;

- (void)stopProgress;

@end
