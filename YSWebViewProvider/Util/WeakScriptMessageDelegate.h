//
//  WeakScriptMessageDelegate.h
//  Childs
//
//  Created by yans on 2016/11/21.
//  Copyright © 2016年 yans. All rights reserved.
//

#import <WebKit/WKScriptMessageHandler.h>

@interface WeakScriptMessageDelegate : NSObject<WKScriptMessageHandler>

@property (nonatomic, weak) id<WKScriptMessageHandler> scriptDelegate;

- (instancetype)initWithDelegate:(id<WKScriptMessageHandler>)scriptDelegate;

@end
