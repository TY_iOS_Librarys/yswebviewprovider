//
//  WVConfigHeader.h
//  YSWebViewProvider
//
//  Created by yans on 2018/5/9.
//

#ifndef WVConfigHeader_h
#define WVConfigHeader_h

#define kWVWindowError @"WVWindowError"

#define kOldProtocolScheme @"wvjbscheme"
#define kNewProtocolScheme @"https"
#define kQueueHasMessage   @"__wvjb_queue_message__"
#define kBridgeLoaded      @"__bridge_loaded__"


#endif /* WVConfigHeader_h */
