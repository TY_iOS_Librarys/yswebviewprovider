//
//  WebViewProgressView.m
//  PlayApp
//
//  Created by yans on 2017/1/19.
//  Copyright © 2017年 hzty. All rights reserved.
//

#import "WebViewProgressView.h"
#import "UIProgressView+WebKit.h"
#import "WebViewJavascriptBridge+NJKWebViewProgressView.h"
#import "NJKWebViewProgress+JavascriptBridge.h"
#import <NJKWebViewProgress/NJKWebViewProgressView.h>
#import "WebViewHelper.h"

@interface WebViewProgressView()<NJKWebViewProgressDelegate>
{
    BOOL addObserver;
    BOOL _stopProgress;
}

/// Current web view url navigation.
@property (nonatomic, strong) WKNavigation *navigation;
/// Progress view.
@property (nonatomic, strong) UIProgressView *wkWebViewProgressView;

/// Progress proxy of progress.
@property (nonatomic, strong) NJKWebViewProgress *uiWebViewProgressProxy;
/// Progress view to show progress of requests.
@property (nonatomic, strong) NJKWebViewProgressView *uiWebViewProgressView;

@property (nonatomic, weak) id<UIWebViewDelegate> targetDelegate;

@property (nonatomic, weak) UIView<FLWebViewProvider,WebViewJavascriptBridgeDelegate,WebViewSharedDelegate> *webView;

@end

@implementation WebViewProgressView

- (void)removeObs
{
    if (addObserver) {
        @try {
            if (_webView) {
                [_webView removeObserver:self forKeyPath:@"estimatedProgress"];
            }
        } @catch (NSException *exception) {
            NSLog(@"WebViewProgressView -removeFromSuperview exception:%@",exception);
        } @finally {
            
        }
        
        addObserver = NO;
    }
}

- (void)addObs
{
    if (!addObserver) {
        [_webView addObserver:self forKeyPath:@"estimatedProgress" options:NSKeyValueObservingOptionNew context:NULL];
        addObserver = YES;
    }
}

- (void)dealloc
{
    [self removeObs];
}

- (instancetype)initWithFrame:(CGRect)frame
                      webView:(UIView<FLWebViewProvider,
                               WebViewJavascriptBridgeDelegate,WebViewSharedDelegate> *)webView
{
    return [self initWithFrame:frame webView:webView targetDelegate:nil];
}

- (instancetype)initWithFrame:(CGRect)frame
                      webView:(UIView<FLWebViewProvider,
                               WebViewJavascriptBridgeDelegate,WebViewSharedDelegate> *)webView
    targetDelegate:(id<UIWebViewDelegate>)targetDelegate
{
    if (self = [super initWithFrame:frame]) {
        
        _webView = webView;
        _targetDelegate = targetDelegate;
        
        if (__IOS8_OR_LATER) {
            [self addSubview:self.wkWebViewProgressView];
            
            [self addObs];
            
            /*
             [_webView.scrollView addObserver:self forKeyPath:@"backgroundColor" options:NSKeyValueObservingOptionNew context:NULL];
             */
        } else {
            [self addSubview:self.uiWebViewProgressView];
            [self progressProxy];
        }
    }
    
    return self;
}

- (void)setFrame:(CGRect)frame
{
    [super setFrame:frame];
    CGRect viewFrame = CGRectMake(0, 0, CGRectGetWidth(frame), CGRectGetHeight(frame));
    if (__IOS8_OR_LATER)
    {
        self.wkWebViewProgressView.frame = viewFrame;
    }
    else{
        self.uiWebViewProgressView.frame = viewFrame;
    }
}

- (void)setProgressTintColor:(UIColor *)progressTintColor
{
    if (__IOS8_OR_LATER)
    {
        self.wkWebViewProgressView.progressTintColor = progressTintColor;
    }
    else{
        self.uiWebViewProgressView.progressBarView.backgroundColor = progressTintColor;
    }
}

- (float)progress
{
    if (__IOS8_OR_LATER)
    {
        return self.wkWebViewProgressView.progress;
    }
    else{
        return self.uiWebViewProgressView.progress;
    }
}

- (void)setProgress:(float)progress
{
    [self setProgress:progress animated:NO];
}

- (void)setProgress:(float)progress animated:(BOOL)animated
{
    if (__IOS8_OR_LATER)
    {
        [self.wkWebViewProgressView setProgress:progress animated:animated];
    }
    else{
        [self.uiWebViewProgressView setProgress:progress animated:animated];
    }
    
    if (self.progressUpdateBlock) {
        self.progressUpdateBlock(progress,animated);
    }
}

- (void)progressProxy
{
    [self uiWebViewProgressProxy];
}

#pragma mark - uiwebview
- (UIProgressView *)wkWebViewProgressView {
    if (_wkWebViewProgressView) return _wkWebViewProgressView;
    CGFloat progressBarHeight = 2.0f;
    CGRect barFrame = CGRectMake(0, 0, self.frame.size.width, progressBarHeight);
    _wkWebViewProgressView = [[UIProgressView alloc] initWithFrame:barFrame];
    _wkWebViewProgressView.trackTintColor = [UIColor clearColor];
    _wkWebViewProgressView.hiddenWhenProgressApproachFullSize = YES;
    _wkWebViewProgressView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
    _wkWebViewProgressView.progress = 0;
    // Set the web view controller to progress view.
    return _wkWebViewProgressView;
}

#pragma mark - uiwebview
- (NJKWebViewProgress *)uiWebViewProgressProxy {
    if (_uiWebViewProgressProxy) return _uiWebViewProgressProxy;
    _uiWebViewProgressProxy = [[NJKWebViewProgress alloc] init];
    
    [self.webView setDelegateViews:_uiWebViewProgressProxy];
    
    _uiWebViewProgressProxy.webViewProxyDelegate = _targetDelegate;
    _uiWebViewProgressProxy.progressDelegate = self;
    
//    self.webView.javascriptBridge = [WebViewJavascriptBridge bridgeUserNJKForWebView:self.webView];
    _uiWebViewProgressProxy.webBridgeDelegate = self.webView.javascriptBridge;
    
    return _uiWebViewProgressProxy;
}

- (NJKWebViewProgressView *)uiWebViewProgressView {
    if (_uiWebViewProgressView) return _uiWebViewProgressView;
    CGFloat progressBarHeight = 2.0f;
    CGRect barFrame = CGRectMake(0, 0, self.frame.size.width, progressBarHeight);
    _uiWebViewProgressView = [[NJKWebViewProgressView alloc] initWithFrame:barFrame];
    _uiWebViewProgressView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
    _uiWebViewProgressView.progress = 0;
    // Set the web view controller to progress view.
    return _uiWebViewProgressView;
}

#pragma mark - NJKWebViewProgressDelegate

- (void)webViewProgress:(NJKWebViewProgress *)webViewProgress updateProgress:(float)progress
{
    [self setProgress:progress animated:YES];
}

- (void)resetWebViewProgress{
    [self setProgress:0 animated:NO];
}

#pragma mark - KVO
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context {
    if ([keyPath isEqualToString:@"estimatedProgress"]) {
        
        if (_stopProgress) {
            return;
        }
        
        float progress = [[change objectForKey:NSKeyValueChangeNewKey] floatValue];
        if (progress >= self.progress) {
            [self setProgress:progress animated:YES];
        } else {
            [self setProgress:progress animated:NO];
        }
        if (progress >= 1.0) {
            [self performSelector:@selector(resetWebViewProgress) withObject:nil afterDelay:0.5];
        }
    } else if ([keyPath isEqualToString:@"backgroundColor"]) {
        if (__IOS8_OR_LATER) {
            /*
             if (![_webView.scrollView.backgroundColor isEqual:[UIColor clearColor]]) {
             _webView.scrollView.backgroundColor = [UIColor clearColor];
             }
             */
        }
    } else {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

- (void)stopProgress
{
    _stopProgress = YES;
    
    if (__IOS8_OR_LATER) {
        [self removeObs];
    } else {
        
    }
    
    [self stopAni];
}

- (void)stopAni
{
    if (__IOS8_OR_LATER)
    {
        if (_wkWebViewProgressView)
        {
            [_wkWebViewProgressView removeFromSuperview];
            [_wkWebViewProgressView.layer removeAllAnimations];
        }
    }
    else
    {
        if (_uiWebViewProgressView)
        {
            [_uiWebViewProgressView removeFromSuperview];
            [_uiWebViewProgressView.layer removeAllAnimations];
        }
    }
}

@end
