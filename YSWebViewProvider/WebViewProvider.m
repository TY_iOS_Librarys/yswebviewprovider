//
//  WebViewProvider.m
//  PlayApp
//
//  Created by yans on 2016/12/23.
//  Copyright © 2016年 hzty. All rights reserved.
//

#import "WebViewProvider.h"
#import "WebViewProvider+UIWebView.h"
#import "WebViewProvider+WKWebView.h"
#import "WVSwzzlingHeader.h"
#import "WKCookieSyncManager.h"
#import "WVConfigHeader.h"
#import "AXPracticalHUD.h"
#import <StoreKit/StoreKit.h>

#define ProgressBarHeight 2.0f


#ifndef kWP404NotFoundHTMLPath
#define kWP404NotFoundHTMLPath                                                 \
    [[NSBundle mainBundle] pathForResource:@"WebView.bundle/html.bundle/404"   \
                                    ofType:@"html"]
#endif
#ifndef kWPNetworkErrorHTMLPath
#define kWPNetworkErrorHTMLPath                                                \
    [[NSBundle mainBundle]                                                     \
        pathForResource:@"WebView.bundle/html.bundle/neterror"                 \
                 ofType:@"html"]
#endif

#define kWPLoadFailTitle @"网络出错，轻触屏幕重新加载"

@interface WebViewProvider () <SKStoreProductViewControllerDelegate> {
    BOOL _loading;
    UIBarButtonItem * __weak _doneItem;
    NSURLRequest *_request;
    UIBarButtonItem * __weak _leftItem;
    NSString *_nameSpace;
    NSMutableArray *goBackBlocks;
    CGRect webviewFrame;
}

/// Navigation back bar button item.
@property (nonatomic, strong) UIBarButtonItem *navigationBackBarButtonItem;
/// Navigation close bar button item.
@property (nonatomic, strong) UIBarButtonItem *navigationCloseBarButtonItem;

@property (nonatomic, strong) WKNavigation *navigation;

@property (nonatomic, strong) NSMutableArray *injectJSList;

/// Array that hold snapshots of pages.
@property(strong, nonatomic) NSMutableArray* snapshots;
/// Current snapshotview displaying on screen when start swiping.
@property(strong, nonatomic) UIView* currentSnapshotView;
/// Previous snapshotview.
@property(strong, nonatomic) UIView* previousSnapshotView;
/// Background alpha black view.
@property(strong, nonatomic) UIView* swipingBackgoundView;
/// Left pan ges.
@property(strong, nonatomic) UIPanGestureRecognizer* swipePanGesture;

@end

@implementation WebViewProvider

- (void)dealloc
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self];

    [self cleanWebView];
    
}

- (void)initializer {
    _maxAllowedTitleLength = 10;
    _useBackCloseButton = YES;
    _rootShowBackButton = YES;
#ifdef DEBUG
    _showWVWindowErrors = YES;
#endif
    
    _useBackButtonTitleSpace = YES;
    _useBackButtonTitle = @"返回";
    _useBackCloseButtonTitle = @"关闭";
}

- (instancetype)init
{
    if (self = [super init]) {
        [self initializer];
    
//        return [self initWithFrame:CGRectZero];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    return [self initWithFrame:frame config:nil];
}

- (instancetype)initWithFrame:(CGRect)frame
                       config:(WKWebViewConfiguration *)config
{
    if (self = [super init]) {
        
        webviewFrame = frame;
        
        [self initializer];
        
        if ((_config = config)) {
            WKUserScript *userScript = [[WKUserScript alloc] initWithSource:injectionJS()
                                                              injectionTime:WKUserScriptInjectionTimeAtDocumentStart
                                                           forMainFrameOnly:NO];

            if (![_config userContentController]) {
                _config.userContentController = [WKUserContentController new];
            }
            [_config.userContentController addUserScript:userScript];
            
            // ad js
            WKUserScript *adCleanScript = [[WKUserScript alloc] initWithSource:adCleanJS()                                                          injectionTime:WKUserScriptInjectionTimeAtDocumentEnd
                                                              forMainFrameOnly:NO];
            [_config.userContentController addUserScript:adCleanScript];

            // wk注入 方式一：
        } else {
            [self.injectJSList addObject:injectionJS()];
            [self.injectJSList addObject:adCleanJS()];

        }

        // Check if WKWebView is available
        // If it is present, create a WKWebView. If not, create a UIWebView.
        if (NSClassFromString(@"WKWebView")) {
            _webView = [self createWKWebViewWithFrame:frame config:_config];
            
            // Obverse the content offset of the scroll view.
            [_webView addObserver:self forKeyPath:@"scrollView.contentOffset" options:NSKeyValueObservingOptionNew context:NULL];
            // Obverse title. Fix issue: https://github.com/devedbox/AXWebViewController/issues/35
            [_webView addObserver:self forKeyPath:@"title" options:NSKeyValueObservingOptionNew context:NULL];
            
            [_webView addObserver:self forKeyPath:@"URL" options:NSKeyValueObservingOptionNew context:NULL];
        } else {
            _webView = [self createUIWebViewWithFrame:frame];
        }
        __weak typeof(self) wkself = self;
        [_webView setDelegateViews:wkself];

        // Ensure that everything will resize on device rotate.
        [_webView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];

        [_webView bridgeForWebViewWithDelegate:wkself];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.01 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            __strong typeof(wkself) sgself = wkself;
            
            if (!sgself.showWVWindowErrors) return;
            
            if (!sgself.registerHandlerWithInjectShort) {
                [sgself addInjectJsCode:generateJSMethod(kWVWindowError)];
            }
            
            [sgself registerHandler:kWVWindowError handler:^(id data, WVJBResponseCallback responseCallback) {
                NSLog(@"%@ -> %@",kWVWindowError,data);
            }];
        });
        
    }
    return self;
}

- (instancetype)initAutoWithFrame:(CGRect)frame
{
    if (NSClassFromString(@"WKWebView")) {

        WKWebViewConfiguration *config = [[WKWebViewConfiguration alloc] init];
        //        config.allowsInlineMediaPlayback = YES;
        //        config.mediaPlaybackRequiresUserAction = YES;
        config.preferences.javaScriptEnabled = YES;
        config.processPool = [WKCookieSyncManager sharedWKCookieSyncManager].processPool;

        config.preferences.minimumFontSize = 9.0;
        if ([config respondsToSelector:@selector(setAllowsInlineMediaPlayback:)]) {
            [config setAllowsInlineMediaPlayback:YES];
        }

        if (__IOS9_OR_LATER) {
            if ([config respondsToSelector:@selector(setApplicationNameForUserAgent:)]) {
                [config setApplicationNameForUserAgent:[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleDisplayName"]];
            }
        }

        if (__IOS10_OR_LATER && [config respondsToSelector:@selector(setMediaTypesRequiringUserActionForPlayback:)]) {
            [config setMediaTypesRequiringUserActionForPlayback:WKAudiovisualMediaTypeNone];
        } else if (__IOS9_OR_LATER && [config respondsToSelector:@selector(setRequiresUserActionForMediaPlayback:)]) {
            [config setRequiresUserActionForMediaPlayback:NO];
        } else if (__IOS8_OR_LATER && [config respondsToSelector:@selector(setMediaPlaybackRequiresUserAction:)]) {
            [config setMediaPlaybackRequiresUserAction:NO];
        }
       
        return [self initWithFrame:frame config:config];
    } else {
        return [self initWithFrame:frame];
    }
}

#pragma mark - KVO
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context {
    if ([keyPath isEqualToString:@"scrollView.contentOffset"]) {
        // Get the current content offset.
        CGPoint contentOffset = [change[NSKeyValueChangeNewKey] CGPointValue];
        _backgroundLabel.transform = CGAffineTransformMakeTranslation(0, -contentOffset.y-_webView.scrollView.contentInset.top);
    } else if ([keyPath isEqualToString:@"title"] ) {
        // Update title of vc.
        [self updateNavigationTitle];
        // And update navigation items if needed.
        [self updateNavigationItems];
    } else if ([keyPath isEqualToString:@"URL"]) {
        // Update title of vc.
        [self updateNavigationTitle];
        // And update navigation items if needed.
        [self updateNavigationItems];

    }
    else {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

#pragma mark -

+ (instancetype)autoWithFrame:(CGRect)frame
{
    return [self autoWithFrame:frame targetController:nil];
}

+ (instancetype)autoWithFrame:(CGRect)frame
             targetController:(UIViewController *)target
{
    WebViewProvider *provider = [[WebViewProvider alloc] initAutoWithFrame:frame];
    provider.targetController = target;
    return provider;
}

#pragma mark -

- (WebViewProgressView *)webViewProgressView
{
    if (_webViewProgressView)
        return _webViewProgressView;
    
    CGRect barFrame = CGRectMake(0, 0, webviewFrame.size.width, ProgressBarHeight);
    if (__IOS8_OR_LATER) {
        _webViewProgressView = [[WebViewProgressView alloc] initWithFrame:barFrame
                                                                  webView:self.webView];
    } else {
        _webViewProgressView = [[WebViewProgressView alloc] initWithFrame:barFrame
                                                                  webView:self.webView
                                                           targetDelegate:self];
    }
    __weak typeof(self) wkself = self;
    [_webViewProgressView setProgressUpdateBlock:^(float progress,BOOL animated){
        __strong typeof(wkself) sgself = wkself;

        if (progress >= 1.0) {
            [sgself updateNavigationItems];
        }
    }];
    
    return _webViewProgressView;
}

- (UILabel *)backgroundLabel
{
    if (_backgroundLabel)
        return _backgroundLabel;
    _backgroundLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    _backgroundLabel.textColor =
        [UIColor colorWithRed:0.322 green:0.322 blue:0.322 alpha:1.00];
    _backgroundLabel.font = [UIFont systemFontOfSize:12];
    _backgroundLabel.numberOfLines = 0;
    _backgroundLabel.textAlignment = NSTextAlignmentCenter;
    _backgroundLabel.backgroundColor = [UIColor clearColor];
    _backgroundLabel.translatesAutoresizingMaskIntoConstraints = NO;
    [_backgroundLabel setContentCompressionResistancePriority:UILayoutPriorityRequired
                                                      forAxis:UILayoutConstraintAxisVertical];
    _backgroundLabel.hidden = YES;
    return _backgroundLabel;
}

- (UIButton *)reloadBtn
{
    if (!_reloadBtn) {
        CGRect frame = self.webView.frame;
        CGRect btnFrame = CGRectMake(0, 0, CGRectGetWidth(frame), CGRectGetHeight(frame));
        _reloadBtn = [[UIButton alloc] initWithFrame:btnFrame];
        [_reloadBtn setTitle:@"加载失败，点击重试" forState:UIControlStateNormal];
        [_reloadBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_reloadBtn setBackgroundColor:[UIColor whiteColor]];
        //        _reloadBtn.center = self.view.center;
        [_reloadBtn addTarget:self action:@selector(failToReload) forControlEvents:UIControlEventTouchUpInside];
    }
    return _reloadBtn;
}

- (UIColor *)__progressTintColor
{
    return _progressTintColor ? _progressTintColor : self.navigationBar.tintColor;
}

#pragma mark -

- (void)setTargetController:(UIViewController *)targetController
{
    _targetController = targetController;

    if (_targetController) {
        if (WebViewProviderUseAutoLayout) {
            [self setupSubviewsByAutoLayout];
        } else {
            [self setupSubviews];
        }

        if (__IOS8_OR_LATER) {
            _backgroundLabel.textColor =
                [UIColor colorWithRed:0.180 green:0.192 blue:0.196 alpha:1.00];
        }

        self.webViewProgressView.progressTintColor = [self __progressTintColor];
    }

    [self setShowProgress:NO];
}

- (void)setTimeoutInternal:(NSTimeInterval)timeoutInternal
{
    _timeoutInternal = timeoutInternal;
    if (__IOS8_OR_LATER) {
        NSMutableURLRequest *request = [_request mutableCopy];
        request.timeoutInterval = _timeoutInternal;
        _navigation = [(WKWebView *)self.webView loadRequest:request];
        _request = [request copy];
    }else{
        NSMutableURLRequest *request = [self.webView.request mutableCopy];
        request.timeoutInterval = _timeoutInternal;
        [_webView loadRequest:request];
    }
}

- (void)setCachePolicy:(NSURLRequestCachePolicy)cachePolicy
{
    _cachePolicy = cachePolicy;
    if (__IOS8_OR_LATER) {
        NSMutableURLRequest *request = [_request mutableCopy];
        request.cachePolicy = _cachePolicy;
        _navigation = [(WKWebView *)self.webView loadRequest:request];
        _request = [request copy];
    }else{
        NSMutableURLRequest *request = [self.webView.request mutableCopy];
        request.cachePolicy = _cachePolicy;
        [_webView loadRequest:request];
    }
}

- (void)setProgressTintColor:(UIColor *)progressTintColor
{
    _progressTintColor = progressTintColor;
    self.webViewProgressView.progressTintColor = [self __progressTintColor];
}

- (void)setShowProgress:(BOOL)showProgress
{
    _showProgress = showProgress;
    self.webViewProgressView.hidden = !_showProgress;
}

- (void)setupSubviews
{
    if (__IOS8_OR_LATER) {
        [self.view addSubview:self.webView];
        
        UIView *contentView = _webView.scrollView.subviews.firstObject;
        [contentView addSubview:self.backgroundLabel];
    } else {
        [self.view insertSubview:self.backgroundLabel atIndex:0];
        [self.view addSubview:self.webView];
    }
    
    [self updateProgressView];
    
    [self updateNavigationItems];
}

- (void)setupSubviewsByAutoLayout
{
    // Add from label and constraints.
    id topLayoutGuide = _targetController.topLayoutGuide;
    id bottomLayoutGuide = _targetController.bottomLayoutGuide;

    // Add web view.
    
    CGFloat webViewHeight = CGRectGetHeight(self.webView.frame);
    NSString *webViewLayout = nil;
    if (webViewHeight > 0) {
        webViewLayout = [NSString stringWithFormat:@"V:[topLayoutGuide][_webView(==%f)]",webViewHeight];
    }else{
        webViewLayout = @"V:[topLayoutGuide][_webView]|";
    }
    
    if (_crossStatusBar) {
        webViewLayout = [webViewLayout stringByReplacingOccurrencesOfString:@"[topLayoutGuide]" withString:@""];
    }
    
    if (!_webViewConstraints) {
        _webViewConstraints = [NSMutableArray array];
    }
    
    for (NSArray<__kindof NSLayoutConstraint *> *constraints in _webViewConstraints) {
        [self.view removeConstraints:constraints];
    }
    
    [_webViewConstraints removeAllObjects];

    if (__IOS8_OR_LATER) {
        [self.view addSubview:self.webView];
        
        [_webViewConstraints addObject:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_webView]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_webView)]];
        [self.view addConstraints:_webViewConstraints[0]];
        [_webViewConstraints addObject:[NSLayoutConstraint constraintsWithVisualFormat:webViewLayout options:0 metrics:nil views:NSDictionaryOfVariableBindings(_webView, topLayoutGuide, bottomLayoutGuide)]];
        
        [self.view addConstraints:_webViewConstraints[1]];
        // Set the content inset of scroll view to the max y position of navigation bar to adjust scroll view content inset.
        // To fix issue: https://github.com/devedbox/AXWebViewController/issues/10
        /*
         UIEdgeInsets contentInset = _webView.scrollView.contentInset;
         contentInset.top = CGRectGetMaxY(self.navigationController.navigationBar.frame);
         _webView.scrollView.contentInset = contentInset;
         */
        
        UIView *contentView = _webView.scrollView.subviews.firstObject;
        [contentView addSubview:self.backgroundLabel];
        [contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[_backgroundLabel(<=width)]" options:0 metrics:@{@"width":@([UIScreen mainScreen].bounds.size.width)} views:NSDictionaryOfVariableBindings(_backgroundLabel)]];
        [contentView addConstraint:[NSLayoutConstraint constraintWithItem:_backgroundLabel attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:contentView attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.0]];
        [contentView addConstraint:[NSLayoutConstraint constraintWithItem:_backgroundLabel attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:contentView attribute:NSLayoutAttributeTop multiplier:1.0 constant:-20]];
    } else {
        [self.view insertSubview:self.backgroundLabel atIndex:0];
        
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-8-[_backgroundLabel]-8-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_backgroundLabel)]];
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[topLayoutGuide]-10-[_backgroundLabel]-(>=0)-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_backgroundLabel, topLayoutGuide)]];
        [self.view addSubview:self.webView];
        
        [_webViewConstraints addObject:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_webView]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_webView)]];
        [self.view addConstraints:_webViewConstraints[0]];
        
        [_webViewConstraints addObject:[NSLayoutConstraint constraintsWithVisualFormat:webViewLayout options:0 metrics:nil views:NSDictionaryOfVariableBindings(_webView, topLayoutGuide, bottomLayoutGuide)]];
        [self.view addConstraints:_webViewConstraints[1]];
    }
    
    [self updateProgressView];
    
    [self updateNavigationItems];
}

- (void)bringProgressViewFront
{
    [self.view bringSubviewToFront:self.webViewProgressView];
}

#pragma mark - nav actions

- (void)updateProgressView
{
    if (self.showProgress) {
        if ([self navigationBarHidden]) {
            self.webViewProgressView.frame = CGRectMake(0, 0, webviewFrame.size.width, 2);
            [self.view addSubview:self.webViewProgressView];
            [self bringProgressViewFront];
        } else {
            CGRect navigationBarBounds = self.navigationBar.bounds;
            self.webViewProgressView.frame = CGRectMake(0, navigationBarBounds.size.height - 2, navigationBarBounds.size.width, 2);
            [self.navigationBar addSubview:self.webViewProgressView];
        }
    }
}

- (void)updateNavigationTitle
{
    [self performSelector:@selector(safeUpdateNavigationTitle) withObject:nil afterDelay:0.5];
}

- (void)safeUpdateNavigationTitle
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(safeUpdateNavigationTitle) object:nil];

    if (!_targetController) {
        return;
    }
    
    NSString *title = [_webView title];
    if (title.length > _maxAllowedTitleLength) {
        title = [[title substringToIndex:_maxAllowedTitleLength-1] stringByAppendingString:@"…"];
    }
    
    if (self.useHtmlTitle)
        self.navigationItem.title = title.length > 0 ? title : @"";

}

- (void)updateNavigationItems
{
    [self performSelector:@selector(safeUpdateNavigationItems) withObject:nil afterDelay:0.5];
}

#pragma mark - left btns

- (void)safeUpdateNavigationItems {
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(safeUpdateNavigationItems) object:nil];
    
    if (!_targetController) {
        return;
    }
    
    if (!_useBackButton) {
        return;
    }
    
    if (!_leftItem) {
        _leftItem = self.navigationItem.leftBarButtonItem;
    }

    [self.navigationItem setLeftBarButtonItems:nil animated:NO];
    if (self.webView.canGoBack/* || self.webView.backForwardList.backItem*/) {// Web view can go back means a lot requests exist.
        UIBarButtonItem *spaceButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
        spaceButtonItem.width = -12;
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
//        if (self.navigationController.viewControllers.count == 1) {
        if (_useBackCloseButton) {
            [self.navigationItem setLeftBarButtonItems:@[spaceButtonItem, self.navigationBackBarButtonItem, self.navigationCloseBarButtonItem] animated:NO];
        }else{
            [self.navigationItem setLeftBarButtonItems:@[spaceButtonItem, self.navigationBackBarButtonItem,] animated:NO];
        }
//        } else {
//            [self.navigationItem setLeftBarButtonItems:@[self.navigationCloseBarButtonItem] animated:NO];
//        }

    } else {
        if (_rootShowBackButton) {
            self.navigationController.interactivePopGestureRecognizer.enabled = YES;
            
            UIBarButtonItem *spaceButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
            spaceButtonItem.width = -12;
            [self.navigationItem setLeftBarButtonItems:@[spaceButtonItem, self.navigationBackBarButtonItem] animated:NO];
            
            //[self.navigationItem setLeftBarButtonItems:nil animated:NO];
        }
    }
}

- (UIBarButtonItem *)navigationBackBarButtonItem {
    if (_navigationBackBarButtonItem) return _navigationBackBarButtonItem;
    
//    UIImage* backItemImage = [[[UINavigationBar appearance] backIndicatorImage] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]?:[[UIImage imageNamed:@"WebView.bundle/backItemImage"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
//    UIGraphicsBeginImageContextWithOptions(backItemImage.size, NO, backItemImage.scale);
//    CGContextRef context = UIGraphicsGetCurrentContext();
//    CGContextTranslateCTM(context, 0, backItemImage.size.height);
//    CGContextScaleCTM(context, 1.0, -1.0);
//    CGContextSetBlendMode(context, kCGBlendModeNormal);
//    CGRect rect = CGRectMake(0, 0, backItemImage.size.width, backItemImage.size.height);
//    CGContextClipToMask(context, rect, backItemImage.CGImage);
//    [[self.navigationController.navigationBar.tintColor colorWithAlphaComponent:0.5] setFill];
//    CGContextFillRect(context, rect);
//    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    UIImage* backItemHlImage = newImage?:[[UIImage imageNamed:@"WebView.bundle/backItemImage-hl"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    UIButton* backButton = [UIButton buttonWithType:UIButtonTypeSystem];
//    NSDictionary *attr = [[UIBarButtonItem appearance] titleTextAttributesForState:UIControlStateNormal];
//    if (attr) {
//        [backButton setAttributedTitle:[[NSAttributedString alloc] initWithString:@"返回" attributes:attr] forState:UIControlStateNormal];
//        UIOffset offset = [[UIBarButtonItem appearance] backButtonTitlePositionAdjustmentForBarMetrics:UIBarMetricsDefault];
//        backButton.titleEdgeInsets = UIEdgeInsetsMake(offset.vertical, offset.horizontal, 0, 0);
//        backButton.imageEdgeInsets = UIEdgeInsetsMake(offset.vertical, offset.horizontal, 0, 0);
//    } else {
    NSString *backButtonTitle = _useBackButtonTitleSpace ? [NSString stringWithFormat:@"  %@ ", _useBackButtonTitle] : _useBackButtonTitle;
    [backButton setTitle:backButtonTitle forState:UIControlStateNormal];
    UIColor *tintColor = [self tintColor];
    backButton.tintColor = tintColor;

        [backButton setTitleColor:tintColor forState:UIControlStateNormal];
        [backButton setTitleColor:[tintColor colorWithAlphaComponent:0.5] forState:UIControlStateHighlighted];
        [backButton.titleLabel setFont:[UIFont systemFontOfSize:17]];
        if (_backIcon) {
            [backButton setImage:_backIcon forState:UIControlStateNormal];

            if (__IOS8_OR_EARLIER) {
                if (__IOS7_OR_EARLIER) {//7.0
                    backButton.titleEdgeInsets = UIEdgeInsetsMake(0, -10, 0, 0);
                }
                else
                {//8.0
                    backButton.titleEdgeInsets = UIEdgeInsetsMake(0, -5, 0, 5);
                }
            }else{
                if (__IOS11_OR_LATER) {//11.0
                    backButton.titleEdgeInsets = UIEdgeInsetsMake(0, -4, 0, 4);
                    //backButton.imageEdgeInsets = UIEdgeInsetsMake(0, -12, 0, 12);
                }
                else
                {
                    if (__IOS10_OR_LATER)
                    {//10.0
                        backButton.imageEdgeInsets = UIEdgeInsetsMake(0, 2, 0, 2);
                        backButton.titleEdgeInsets = UIEdgeInsetsMake(0, -12, 0, 0);
                    }
                    else
                    {//9.0
                        backButton.imageEdgeInsets = UIEdgeInsetsMake(0, -2, 0, 2);
                        backButton.titleEdgeInsets = UIEdgeInsetsMake(0, -12, 0, 0);
                    }
                }
            }
        } else {
            backButton.titleEdgeInsets = UIEdgeInsetsMake(0, -12, 0, 0);
        }
    
    [backButton sizeToFit];

//    }
//    [backButton setImage:backItemImage forState:UIControlStateNormal];
//    [backButton setImage:backItemHlImage forState:UIControlStateHighlighted];
    
    [backButton addTarget:self action:@selector(navigationItemHandleBack:) forControlEvents:UIControlEventTouchUpInside];
    _navigationBackBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    _navigationBackBarButtonItem.tintColor = tintColor;
//    _navigationBackBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"返回" style:UIBarButtonItemStyleBordered target:self action:@selector(navigationItemHandleBack:)];
//    _navigationBackBarButtonItem.tintColor = self.navigationController.navigationBar.tintColor;
    return _navigationBackBarButtonItem;
}

- (UIColor *)tintColor
{
    UIColor *tintColor = _navigationBarTintColor;
    if (!tintColor) {
        tintColor =  self.navigationController.navigationBar.tintColor;
    }
    return tintColor;
}

- (UIBarButtonItem *)navigationCloseBarButtonItem {
    if (_navigationCloseBarButtonItem) return _navigationCloseBarButtonItem;
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    UIColor *tintColor = [self tintColor];
    btn.tintColor = tintColor;
    [btn setTitleColor:tintColor forState:UIControlStateNormal];
    [btn setTitleColor:[tintColor colorWithAlphaComponent:0.5] forState:UIControlStateHighlighted];
    [btn setTitle:_useBackCloseButtonTitle forState:UIControlStateNormal];
    

    [btn.titleLabel setFont:[UIFont systemFontOfSize:17]];
    
    if (__IOS8_OR_EARLIER) {
        if (__IOS7_OR_EARLIER) {//7.0
            btn.titleEdgeInsets = UIEdgeInsetsMake(0, -8, 0, 8);
        }
        else
        {//8.0
            btn.titleEdgeInsets = UIEdgeInsetsMake(0, -10, 0, 10);
        }
    }else{
        if (__IOS11_OR_LATER) {//11.0
            btn.titleEdgeInsets = UIEdgeInsetsMake(0, -8, 0, 8);
        }else{//9.0 10.0
            btn.titleEdgeInsets = UIEdgeInsetsMake(0, -10, 0, 10);
        }
    }
    
    [btn sizeToFit];

    if (self.navigationItem.rightBarButtonItem == _doneItem && self.navigationItem.rightBarButtonItem != nil) {
        [btn addTarget:self action:@selector(doneButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    } else {
        [btn addTarget:self action:@selector(navigationIemHandleClose:) forControlEvents:UIControlEventTouchUpInside];
    }
    _navigationCloseBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btn];
    _navigationCloseBarButtonItem.tintColor = tintColor;

    return _navigationCloseBarButtonItem;
}

- (void)navigationItemHandleBack:(UIBarButtonItem *)sender {
    
    if (self.leftBtnBlock) {
        BOOL needReturn = self.leftBtnBlock();
        if (needReturn) {
            return;
        }
    }
    
    if ([self.webView canGoBack]) {
        if (__IOS8_OR_LATER) {
            
            [self callGoBackBlocks:NO];
            
            _navigation = [(WKWebView *)self.webView goBack];
        }
        else
        {
            [self callGoBackBlocks:NO];

            [(UIWebView *)self.webView goBack];
        }
        return;
    }
    
    [self cleanBackAction];
    
}

- (void)callGoBackBlocks:(BOOL)close
{
    if (self.goBackBlock) {
        self.goBackBlock(close);
    }
    
    if (goBackBlocks) {
        for (void(^block)(BOOL) in goBackBlocks) {
            block(close);
        }
    }
}

- (void)cleanBackAction
{
    [self callGoBackBlocks:YES];
    
    [goBackBlocks removeAllObjects];
    
    [self stopPlayingMedia];
    
    [self cleanWebView];
    
    if (self.closeBlock) {
        self.closeBlock();
    } else {
        if (self.isPresent) {
            [self.targetController dismissViewControllerAnimated:YES completion:nil];
        } else {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}

- (void)stopPlayingMedia
{
    NSString *pauseJSString = @"{\
    (function (){\
    var videos = document.getElementsByTagName('video');\
    var len = videos.length;\
    for(var i=0;i<len;i++){\
    videos[i].pause();\
    }\
    })();\
    (function (){\
    var audios = document.getElementsByTagName('audio');\
    var len = audios.length;\
    for(var i=0;i<len;i++){\
    audios[i].pause();\
    }\
    })();\
    };";

    [self evaluateJavaScript:pauseJSString completionHandler:nil];
}

- (void)cleanWebView
{
    if (_webViewProgressView) {
        [_webViewProgressView setAlpha:0];
        [_webViewProgressView stopProgress];
        [_webViewProgressView setHidden:YES];
        [_webViewProgressView.layer removeAllAnimations];
        [_webViewProgressView removeFromSuperview];
        _webViewProgressView = nil;
    }
    
    if (_webView) {
        if (_webViewProgressView) {//先让看不见
            [_webViewProgressView setBackgroundColor:[UIColor clearColor]];
        }
        
        [_webView setDelegateViews:nil];
        _webView.scrollView.delegate = nil;
        
        [_webView stopLoading];
        
        if (__IOS8_OR_LATER) {
            @try{
                [_webView removeObserver:self forKeyPath:@"scrollView.contentOffset"];
                [_webView removeObserver:self forKeyPath:@"title"];
                [_webView removeObserver:self forKeyPath:@"URL"];
            }
            @catch(NSException *exception) {
                NSLog(@"WebViewProvider dealloc exception : %@", exception);
            }
            @finally {
                
            }
        }

        [_webView removeFromSuperview];
        _webView = nil;
    }
    
}

- (void)doneButtonClicked:(id)sender {
    [self.targetController dismissViewControllerAnimated:YES completion:NULL];
}

- (void)navigationIemHandleClose:(UIBarButtonItem *)sender {
    [self cleanBackAction];

}

#pragma mark -

- (UIView *)view
{
    return _targetController.view;
}

- (BOOL)navigationBarHidden
{
    if (!self.navigationController) {
        return YES;
    }
    return self.navigationController.navigationBarHidden;
}

- (UINavigationController *)navigationController
{
    return self.targetController.navigationController;
}

- (UINavigationItem *)navigationItem
{
    if ([_targetController respondsToSelector:@selector(navigationItem)]) {
        return _targetController.navigationItem;
    }
    return self.navigationController.navigationItem;
}

- (UINavigationBar *)navigationBar
{
    return self.navigationController.navigationBar;
}

- (NSMutableArray *)injectJSList
{
    if (!_injectJSList) {
        _injectJSList = @[].mutableCopy;
    }
    return _injectJSList;
}


#pragma mark - load fail view

- (UIView *)loadFailView {
    if (!_loadFailView) {
        _loadFailView = [[UIView alloc] initWithFrame:CGRectMake(0, 20, CGRectGetWidth(self.webView.frame), CGRectGetHeight(self.webView.frame) - 20)];
        _loadFailView.backgroundColor = [UIColor whiteColor];
        [_loadFailView addSubview:self.loadFailRefreshImageView];
        [_loadFailView addSubview:self.loadFailRefreshLabel];
        UIGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(failToReload)];
        [_loadFailView addGestureRecognizer:gesture];
    }
    return _loadFailView;
}

- (void)failToReload
{
    if (_reloadBtn) {
        [_reloadBtn removeFromSuperview];
    }
    
    if (_loadFailView) {
        [_loadFailView removeFromSuperview];
    }
    
    [self performSelector:@selector(reload) withObject:nil afterDelay:0.1];
}

- (UIImageView *)loadFailRefreshImageView {
    if (!_loadFailRefreshImageView) {
        _loadFailRefreshImageView = [[UIImageView alloc] init];
        _loadFailRefreshImageView.frame = CGRectMake((CGRectGetWidth(self.webView.frame) - 120) / 2, 120, 120, 120);
        NSString *imagePath = [[NSBundle mainBundle] pathForResource:@"WebView.bundle/WebView_LoadFail_Refresh_Icon@2x" ofType:@"png"];
        _loadFailRefreshImageView.image = [UIImage imageWithContentsOfFile:imagePath];
    }
    return _loadFailRefreshImageView;
}

- (UILabel *)loadFailRefreshLabel {
    if (!_loadFailRefreshLabel) {
        _loadFailRefreshLabel = [[UILabel alloc] init];
        _loadFailRefreshLabel.textColor = [UIColor lightGrayColor];
        _loadFailRefreshLabel.textAlignment = NSTextAlignmentCenter;
        _loadFailRefreshLabel.font = [UIFont systemFontOfSize:12];
        CGFloat y = CGRectGetMaxY(self.loadFailRefreshImageView.frame) + 20;
        _loadFailRefreshLabel.frame = CGRectMake(0, y, CGRectGetWidth(self.webView.frame), _loadFailRefreshLabel.font.pointSize);
    }
    return _loadFailRefreshLabel;
}

- (void)updateLoadFailRefreshLabelTitle:(NSString *)title
{
    _loadFailRefreshLabel.text = title;
}

#pragma mark - Shared Delegate Methods

/*
 * This is called whenever the web view wants to navigate.
 */
- (BOOL)shouldStartDecidePolicy:(NSURLRequest *)request
{
    // Determine whether or not navigation should be allowed.
    // Return YES if it should, NO if not.
    [self updateNavigationItems];

    if (self.shouldStartDecidePolicy) {
        return self.shouldStartDecidePolicy(request);
    }

    return YES;
}

/*
 * This is called whenever the web view has started navigating.
 */
- (void)didStartNavigation:(id)webView
{
    // Update things like loading indicators here.
    // uiwebview 注入js ..
    //[self runInjectJsCode];

    [self didStartLoad];

    if (self.didStartNavigation) {
        self.didStartNavigation();
    }

    UIView<FLWebViewProvider> *webView_ = webView;
    if (self.didStartNavigationRequest) {
        self.didStartNavigationRequest([webView_ request]);
    }
}

/*
 * This is called when navigation failed.
 */
- (void)failLoadOrNavigation:(NSURLRequest *)request withError:(NSError *)error
{
    // Notify the user that navigation failed, provide information on the error,
    // and so on.
    [self didFailLoadWithError:error];

    if (self.failLoadOrNavigation) {
        self.failLoadOrNavigation(request, error);
    }
}

/*
 * This is called when navigation succeeds and is complete.
 */
- (void)finishLoadOrNavigation:(NSURLRequest *)request
{
    [self runInjectJsCode];

    [self runJsReadysCode];

    [self didFinishLoad];
    
    if (_showReloadBtn) {
        if (_reloadBtn) {
            [_reloadBtn removeFromSuperview];
        }
    }
    
    if (_showLoadFailView) {
        if (_loadFailView) {
            [_loadFailView removeFromSuperview];
        }
    }

    // Remove the loading indicator, maybe update the navigation bar's title if
    // you have one.
    if (self.finishLoadOrNavigation) {
        self.finishLoadOrNavigation(request);
    }
}

//2017.4.6
- (void) runJavaScriptPanelWithMessage: (NSString *) message alertController: (UIAlertController *) alert
{
    if (self.runJavaScriptPanelWithMessage) {
        self.runJavaScriptPanelWithMessage(message,alert);
    }
}

#pragma mark -
- (void)didStartLoad
{
    _loading = YES;
    
    _backgroundLabel.text = @"加载中...";
    
    if (_targetController) {
        [self updateProgressView];
        
        if (self.useHtmlTitle)
        self.navigationItem.title = @"加载中...";
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
        
        if (__IOS7_OR_EARLIER) {
            _webViewProgressView.progress = 0.0;
        }
        
        /*
         _updating = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self
         selector:@selector(updatingProgress:) userInfo:nil repeats:YES];
         */
        
        [self updateNavigationItems];
    }
    
}

- (void)didFinishLoad
{
    @try {
        [self hookWebContentCommitPreviewHandler];
    } @catch (NSException *exception) {
    } @finally {
    }
    
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString *bundle = ([infoDictionary objectForKey:@"CFBundleDisplayName"] ?: [infoDictionary objectForKey:@"CFBundleName"]) ?: [infoDictionary objectForKey:@"CFBundleIdentifier"];

    NSString *host = _webView.host;

    _backgroundLabel.text = [NSString stringWithFormat:@"%@\"%@\"%@.", @"网页由 ", host ? : bundle, @"提供"];

    _loading = NO;
    
    if (_targetController) {
        [self updateNavigationItems];
        
        [self updateNavigationTitle];
        
        if (__IOS7_OR_EARLIER) {
            [_webViewProgressView setProgress:0.9 animated:YES];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                if (_webViewProgressView.progress != 1.0) {
                    [_webViewProgressView setProgress:1.0 animated:YES];
                }
            });
        }
    }
}

- (void)didFailLoadWithError:(NSError *)error
{
    if (_showReloadBtn) {
        [self.webView addSubview:self.reloadBtn];
        [self bringProgressViewFront];
    }
    
    if (_showLoadFailHtmlPage) {
        if (error.code == NSURLErrorCannotFindHost) {// 404
            [self loadURL:[NSURL fileURLWithPath:kWP404NotFoundHTMLPath]];
        } else {
            [self loadURL:[NSURL fileURLWithPath:kWPNetworkErrorHTMLPath]];
        }
    }
    
    if (_showLoadFailView) {
        [self.webView addSubview:self.loadFailView];
        NSString *failTitle = [NSString stringWithFormat:@"%@:%@",kWPLoadFailTitle,@(error.code)];
        [self updateLoadFailRefreshLabelTitle:failTitle];
        [self bringProgressViewFront];
    }

    _backgroundLabel.text = [NSString stringWithFormat:@"%@%@", @"网页加载失败：", error.localizedDescription];

    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];

    if (_targetController) {
        if (self.useHtmlTitle) {
            self.navigationItem.title = @"加载失败";
        }
        
        [self updateNavigationItems];

        
        [_webViewProgressView setProgress:0.9 animated:YES];
    }
}

#pragma mark -

+ (void)clearWebCacheCompletion:(dispatch_block_t)completion
{
    if (__IOS9_OR_LATER) {
        NSSet *websiteDataTypes = [WKWebsiteDataStore allWebsiteDataTypes];
        NSDate *dateFrom = [NSDate dateWithTimeIntervalSince1970:0];
        [[WKWebsiteDataStore defaultDataStore] removeDataOfTypes:websiteDataTypes modifiedSince:dateFrom completionHandler:completion];
    } else {
        NSString *libraryDir = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES)[0];
        NSString *bundleId  =  [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleIdentifier"];
        NSString *webkitFolderInLib = [NSString stringWithFormat:@"%@/WebKit",libraryDir];
        NSString *webKitFolderInCaches = [NSString stringWithFormat:@"%@/Caches/%@/WebKit",libraryDir,bundleId];
        NSString *webKitFolderInCachesfs = [NSString stringWithFormat:@"%@/Caches/%@/fsCachedData",libraryDir,bundleId];
        
        NSError *error;
        /* iOS8.0 WebView Cache path */
        [[NSFileManager defaultManager] removeItemAtPath:webKitFolderInCaches error:&error];
        [[NSFileManager defaultManager] removeItemAtPath:webkitFolderInLib error:nil];
        
        /* iOS7.0 WebView Cache path */
        [[NSFileManager defaultManager] removeItemAtPath:webKitFolderInCachesfs error:&error];
        if (completion) {
            completion();
        }
    }
}

#pragma mark -

- (void)runInjectJsCode
{
    if (self.injectJSList.count) {
        NSString *jsCode = [self.injectJSList componentsJoinedByString:@";"];
        __weak typeof(self) wkself = self;
        [self evaluateJavaScript:jsCode
               completionHandler:^(id data, NSError *error){
                   __strong typeof(wkself) sgself = wkself;

                   if (sgself.injectJsCodeBlock) {
                       sgself.injectJsCodeBlock(data, error);
                   }
               }];
    }
}

- (void)runJsReadysCode
{
    [self evaluateJavaScript:runReadyJS()
           completionHandler:^(id data, NSError *error){
           }];
}

#pragma mark -

- (void)addGoBackBlocks:(void(^)(BOOL))block
{
    if (!goBackBlocks) {
        goBackBlocks = [NSMutableArray array];
    }
    
    [goBackBlocks addObject:block];
}

- (void)addNameSpace:(NSString *)nameSpace
{
    _nameSpace = nameSpace;
    if (nameSpace) {
        [self addInjectJsCode:generateGlobalNameSpace(nameSpace)];
    }
}

- (void)addInjectJsCode:(NSString *)jsContent
{
    if (jsContent) {
        [self.injectJSList addObject:jsContent];
    }
}

- (void)addInjectJsCodeRun:(NSString *)code
{
    WKUserScript *userScript = [[WKUserScript alloc] initWithSource:code
                                                      injectionTime:WKUserScriptInjectionTimeAtDocumentStart
                                                   forMainFrameOnly:NO];
    
    if (![_config userContentController]) {
        _config.userContentController = [WKUserContentController new];
    }
    [_config.userContentController addUserScript:userScript];

}

#pragma mark -

- (void)registerHandler:(NSString *)handlerName handler:(WVJBHandler)handler
{
    if (_registerHandlerWithInjectShort) {
        [self addInjectJsCode:generateJSMethod(handlerName)];
    }
    
    [self.webView registerHandler:handlerName handler:handler];
}

- (NSString *)registerHandlerForOnloadCall:(NSString *)handlerName handler:(WVJBHandler)handler
{
    return [self registerHandlerForOnloadCall:handlerName nameSpace:_nameSpace handler:handler];
}

- (NSString *)registerHandlerForOnloadCall:(NSString *)handlerName nameSpace:(NSString *)nameSpace handler:(WVJBHandler)handler
{
    //转换伪方法
    NSString *placehandlerName = [NSString stringWithFormat:@"%@_forOnloadCall",handlerName];
    NSString *prepareCallJs = prepareRegistrMethodJS(handlerName, placehandlerName, nameSpace);
    
    if (__IOS8_OR_LATER) {
        [self addInjectJsCodeRun:prepareCallJs];
    }
    
    [self evaluateJavaScript:prepareCallJs completionHandler:^(id data, NSError *error) {

    }];
    //生成短方法
    if (_registerHandlerWithInjectShort) {
        [self addInjectJsCode:generateJSMethodWithNP(placehandlerName,nameSpace)];
    }
    [self.webView registerHandler:placehandlerName handler:handler];

    //这里还是补一次
    if (_registerHandlerWithInjectShort) {
        [self addInjectJsCode:generateJSMethodWithNP(handlerName,nameSpace)];
    }
    [self.webView registerHandler:handlerName handler:handler];
    return placehandlerName;
}

#pragma mark -

- (void)callHandler:(NSString *)handlerName
                data:(id)data
    responseCallback:(WVJBResponseCallback)responseCallback
{
    [self.webView callHandler:handlerName
                         data:data
             responseCallback:responseCallback];
}

- (void)callSelfHandler:(NSString *)handlerName
               data:(id)data
   callback:(void(^)(id, NSError *))callback
{
    [self evaluateMethod:handlerName data:data completionHandler:callback];
}

#pragma mark -

- (void)evaluateMethod:(NSString *)methodName data:(id)data
         completionHandler:(void (^)(id, NSError *))completionHandler
{
    NSString *params = nil;
    if (data) {
        if ([data isKindOfClass:[NSString class]])
        {
            params = [NSString stringWithFormat:@"'%@'",data];
        }
        else if ([data isKindOfClass:[NSNumber class]])
        {
            params = [NSString stringWithFormat:@"%@",data];
        }
        else// if ([data isKindOfClass:[NSArray class]] || [data isKindOfClass:[NSDictionary class]])
        {
            NSError *error;
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:data options:NSJSONWritingPrettyPrinted error:&error];
            if (!error) {
                params = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            }else{
                NSLog(@"callSelfHandler params error :%@",error);
            }
        }
    }
    NSString *js = nil;
    if (params) {
        js = [NSString stringWithFormat:@"%@(%@)", methodName, params];
    }else{
        js = [NSString stringWithFormat:@"%@()", methodName];
    }
    
    js = [NSString stringWithFormat:@"try{ %@ }catch(e){ console.log('%@ error ==> '+e); }",js,methodName];
    
    [self evaluateJavaScript:js completionHandler:completionHandler];
}

- (void)evaluateJavaScript:(NSString *)javaScriptString
         completionHandler:(void (^)(id, NSError *))completionHandler
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.webView evaluateJavaScript:javaScriptString
                       completionHandler:completionHandler];
    });
}

#pragma mark -

- (void)loadRequest:(NSURLRequest *)request
{
    _request = request;
    
    [self updateURL:[_request URL]];
    
    [self.webView loadRequest:_request];
}

- (void)loadHTMLString:(NSString *)string baseURL:(NSURL *)baseURL
{
    [self.webView loadHTMLString:string baseURL:baseURL];
}

- (void)loadURLString:(NSString *)urlString
{
    NSURL *url = [NSURL URLWithString:urlString];
    [self loadURL:url];
}

- (void)loadURL:(NSURL *)url
{
    NSURLRequest *request;
    if (__IOS7_OR_EARLIER) {
        request = [[NSURLRequest alloc] initWithURL:url
                                        cachePolicy:NSURLRequestReloadRevalidatingCacheData
                                    timeoutInterval:30.0];
    } else {
        request = [[NSURLRequest alloc] initWithURL:url];
    }
    
    [self updateURL:url];
    
    [self loadRequest:request];
}

- (void)updateURL:(NSURL *)url
{
    if (![[NSPredicate predicateWithFormat:@"SELF ENDSWITH[cd] %@ OR SELF ENDSWITH[cd] %@", kWP404NotFoundHTMLPath, kWPNetworkErrorHTMLPath] evaluateWithObject:url.absoluteString]) {
        _URL = url;
    }
}

#pragma mark -

- (void)reload
{
    if ([self.webView URL]) {
        [self.webView reload];
    }else{
        [self loadURL:self.URL];
    }
}

- (void)goBack
{
    [self.webView goBack];
}

- (void)goForward
{
    [self.webView goForward];
}

- (void)stopLoading
{
    [self.webView stopLoading];
}

#pragma mark - 

- (void)hookWebContentCommitPreviewHandler {
// todo...
}


#pragma mark - 

#if __IPHONE_OS_VERSION_MIN_REQUIRED < __IPHONE_8_0
-(void)pushCurrentSnapshotViewWithRequest:(NSURLRequest*)request{
    NSURLRequest* lastRequest = (NSURLRequest*)[[self.snapshots lastObject] objectForKey:@"request"];
    
    // 如果url是很奇怪的就不push
    if ([request.URL.absoluteString isEqualToString:@"about:blank"]) {
        return;
    }
    //如果url一样就不进行push
    if ([lastRequest.URL.absoluteString isEqualToString:request.URL.absoluteString]) {
        return;
    }
    
    UIView* currentSnapshotView = [self.webView snapshotViewAfterScreenUpdates:YES];
    [self.snapshots addObject:
     @{@"request":request,
       @"snapShotView":currentSnapshotView}
     ];
}

-(void)startPopSnapshotView{
    if (self.isSwipingBack) {
        return;
    }
    if (!self.webView.canGoBack) {
        return;
    }
    self.isSwipingBack = YES;
    //create a center of scrren
    CGPoint center = CGPointMake(self.view.bounds.size.width/2, self.view.bounds.size.height/2);
    
    self.currentSnapshotView = [self.webView snapshotViewAfterScreenUpdates:YES];
    
    //add shadows just like UINavigationController
    self.currentSnapshotView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.currentSnapshotView.layer.shadowOffset = CGSizeMake(3, 3);
    self.currentSnapshotView.layer.shadowRadius = 5;
    self.currentSnapshotView.layer.shadowOpacity = 0.75;
    
    //move to center of screen
    self.currentSnapshotView.center = center;
    
    self.previousSnapshotView = (UIView*)[[self.snapshots lastObject] objectForKey:@"snapShotView"];
    center.x -= 60;
    self.previousSnapshotView.center = center;
    self.previousSnapshotView.alpha = 1;
    self.view.backgroundColor = [UIColor colorWithRed:0.180 green:0.192 blue:0.196 alpha:1.00];
    
    [self.view addSubview:self.previousSnapshotView];
    [self.view addSubview:self.swipingBackgoundView];
    [self.view addSubview:self.currentSnapshotView];
}

-(void)popSnapShotViewWithPanGestureDistance:(CGFloat)distance{
    if (!self.isSwipingBack) {
        return;
    }
    
    if (distance <= 0) {
        return;
    }
    
    CGFloat boundsWidth = CGRectGetWidth(self.view.bounds);
    CGFloat boundsHeight = CGRectGetHeight(self.view.bounds);
    
    CGPoint currentSnapshotViewCenter = CGPointMake(boundsWidth/2, boundsHeight/2);
    currentSnapshotViewCenter.x += distance;
    CGPoint previousSnapshotViewCenter = CGPointMake(boundsWidth/2, boundsHeight/2);
    previousSnapshotViewCenter.x -= (boundsWidth - distance)*60/boundsWidth;
    
    self.currentSnapshotView.center = currentSnapshotViewCenter;
    self.previousSnapshotView.center = previousSnapshotViewCenter;
    self.swipingBackgoundView.alpha = (boundsWidth - distance)/boundsWidth;
}

-(void)endPopSnapShotView{
    if (!self.isSwipingBack) {
        return;
    }
    
    //prevent the user touch for now
    self.view.userInteractionEnabled = NO;
    
    CGFloat boundsWidth = CGRectGetWidth(self.view.bounds);
    CGFloat boundsHeight = CGRectGetHeight(self.view.bounds);
    
    if (self.currentSnapshotView.center.x >= boundsWidth) {
        // pop success
        [UIView animateWithDuration:0.2 animations:^{
            [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
            
            self.currentSnapshotView.center = CGPointMake(boundsWidth*3/2, boundsHeight/2);
            self.previousSnapshotView.center = CGPointMake(boundsWidth/2, boundsHeight/2);
            self.swipingBackgoundView.alpha = 0;
        }completion:^(BOOL finished) {
            [self.previousSnapshotView removeFromSuperview];
            [self.swipingBackgoundView removeFromSuperview];
            [self.currentSnapshotView removeFromSuperview];
            [(WKWebView *)self.webView goBack];
            [self.snapshots removeLastObject];
            self.view.userInteractionEnabled = YES;
            
            self.isSwipingBack = NO;
        }];
    }else{
        //pop fail
        [UIView animateWithDuration:0.2 animations:^{
            [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
            
            self.currentSnapshotView.center = CGPointMake(boundsWidth/2, boundsHeight/2);
            self.previousSnapshotView.center = CGPointMake(boundsWidth/2-60, boundsHeight/2);
            self.previousSnapshotView.alpha = 1;
        }completion:^(BOOL finished) {
            [self.previousSnapshotView removeFromSuperview];
            [self.swipingBackgoundView removeFromSuperview];
            [self.currentSnapshotView removeFromSuperview];
            self.view.userInteractionEnabled = YES;
            
            self.isSwipingBack = NO;
        }];
    }
}

- (UIView*)swipingBackgoundView{
    if (!_swipingBackgoundView) {
        _swipingBackgoundView = [[UIView alloc] initWithFrame:self.view.bounds];
        _swipingBackgoundView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.3];
    }
    return _swipingBackgoundView;
}

- (NSMutableArray*)snapshots{
    if (!_snapshots) {
        _snapshots = [NSMutableArray array];
    }
    return _snapshots;
}

- (BOOL)isSwipingBack{
    if (!_isSwipingBack) {
        _isSwipingBack = NO;
    }
    return _isSwipingBack;
}

- (UIPanGestureRecognizer*)swipePanGesture{
    if (!_swipePanGesture) {
        _swipePanGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(swipePanGestureHandler:)];
    }
    return _swipePanGesture;
}

- (void)swipePanGestureHandler:(UIPanGestureRecognizer*)panGesture{
    CGPoint translation = [panGesture translationInView:self.webView];
    CGPoint location = [panGesture locationInView:self.webView];
    
    if (panGesture.state == UIGestureRecognizerStateBegan) {
        if (location.x <= 50 && translation.x >= 0) {  //开始动画
            [self startPopSnapshotView];
        }
    }else if (panGesture.state == UIGestureRecognizerStateCancelled || panGesture.state == UIGestureRecognizerStateEnded){
        [self endPopSnapShotView];
    }else if (panGesture.state == UIGestureRecognizerStateChanged){
        [self popSnapShotViewWithPanGestureDistance:translation.x];
    }
}
#endif

#pragma mark - SKStoreProductViewControllerDelegate.
- (void)productViewControllerDidFinish:(SKStoreProductViewController *)viewController {
    if (viewController) {
        [viewController dismissViewControllerAnimated:YES completion:NULL];
    }
}


#pragma mark -

+ (void)applicationOpenURL:(NSURL *)url completionHandler:(void (^ __nullable)(BOOL success))completion
{
    if ([[UIApplication sharedApplication] canOpenURL:url]) {
        if (__IOS10_OR_LATER/*UIDevice.currentDevice.systemVersion.floatValue >= 10.0*/) {
            [UIApplication.sharedApplication openURL:url options:@{} completionHandler:completion];
        } else {
            BOOL ret = [[UIApplication sharedApplication] openURL:url];
            if (completion) {
                completion(ret);
            }
        }
    }
}

- (BOOL)findProductInfoWithUrl:(NSString *)ursString
{
    [[AXPracticalHUD sharedHUD] showNormalInView:[[[UIApplication sharedApplication] delegate] window] text:nil detail:nil configuration:^(AXPracticalHUD *HUD) {
        HUD.lockBackground = YES;
        HUD.removeFromSuperViewOnHide = YES;
    }];
    NSError *error;
    NSRegularExpression *regex = [[NSRegularExpression alloc] initWithPattern:@"id[1-9]\\d*" options:NSRegularExpressionCaseInsensitive error:&error];
    NSTextCheckingResult *result = [regex firstMatchInString:ursString options:NSMatchingReportCompletion range:NSMakeRange(0, ursString.length)];
    
    if (!error && result) {
        NSRange range = NSMakeRange(result.range.location+2, result.range.length-2);
        NSInteger productId = [[ursString substringWithRange:range] integerValue];
        
        [self showProductViewController:productId];
        
        return YES;
    } else {
        [[AXPracticalHUD sharedHUD] hide:YES afterDelay:0.5 completion:NULL];
        return NO;
    }
}

- (void)showProductViewController:(NSInteger)pruductId
{
    SKStoreProductViewController *productVC = [[SKStoreProductViewController alloc] init];
    productVC.delegate = self;
    __weak typeof(self) wkself = self;
    [productVC loadProductWithParameters:@{SKStoreProductParameterITunesItemIdentifier:@(pruductId)} completionBlock:^(BOOL result, NSError * _Nullable error) {
        __strong typeof(wkself) sgself = wkself;
        
        if (!result || error) {
            [[AXPracticalHUD sharedHUD] showErrorInView:[[[UIApplication sharedApplication] delegate] window] text:error.localizedDescription detail:nil configuration:^(AXPracticalHUD *HUD) {
                HUD.lockBackground = YES;
                HUD.removeFromSuperViewOnHide = YES;
            }];
            [[AXPracticalHUD sharedHUD] hide:YES afterDelay:1.5 completion:NULL];
        } else {
            [[AXPracticalHUD sharedHUD] hide:YES afterDelay:0.5 completion:NULL];
            
            UIViewController *rootViewController = [[[UIApplication sharedApplication] delegate] window].rootViewController;
            if (rootViewController) {
                [rootViewController presentViewController:productVC animated:YES completion:NULL];
            } else {
                [[sgself targetController] presentViewController:productVC animated:YES completion:NULL];
            }
        }
    }];
}

@end
