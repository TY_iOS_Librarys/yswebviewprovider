//
//  WebViewJavascriptBridgeDelegate.h
//  PlayApp
//
//  Created by YANS on 2016/12/22.
//  Copyright © 2016年 hzty. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WebViewJavascriptBridgeBase.h"

@protocol WebViewJavascriptBridgeDelegate <NSObject>

@property (nonatomic, strong) id javascriptBridge;

@optional

- (void)bridgeForWebViewWithDelegate:(id)delegate;

- (void)setJavaScriptBridgeWebViewDelegate:(id)delegate;

- (void)registerHandler:(NSString *)handlerName handler:(WVJBHandler)handler;

- (void)callHandler:(NSString *)handlerName data:(id)data responseCallback:(WVJBResponseCallback)responseCallback;



@end
