//
//  WebViewProviderProtocol.h
//  PlayApp
//
//  Created by yans on 2017/1/19.
//  Copyright © 2017年 hzty. All rights reserved.
//


@protocol WebViewProviderSharedDelegate <NSObject>

- (BOOL) shouldStartDecidePolicy: (NSURLRequest *) request;

- (void) didStartNavigation:(id)webView;

- (void) failLoadOrNavigation: (NSURLRequest *) request withError: (NSError *) error;

- (void) finishLoadOrNavigation: (NSURLRequest *) request;

- (void) runJavaScriptPanelWithMessage: (NSString *) message alertController: (UIAlertController *) alert;//2017.4.6

@end

