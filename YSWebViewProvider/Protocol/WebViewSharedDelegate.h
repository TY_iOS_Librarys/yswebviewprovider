//
//  WebViewSharedDelegate.h
//  PlayApp
//
//  Created by yans on 2017/1/19.
//  Copyright © 2017年 hzty. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol WebViewSharedDelegate <NSObject>

- (void)stopLoading;

- (NSString *)title;

- (UIScrollView *)scrollView;

- (NSString *)host;

//加载本地
- (void)loadHTMLString:(NSString *)string baseURL:(NSURL *)baseURL;

- (void)reload;

- (void)goBack;
- (void)goForward;


@end


