//
//  NSURLProtocol+WebKitSupport.h
//  Student
//
//  Created by yans on 2017/10/24.
//  Copyright © 2017年 yans. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSURLProtocol (WebKitSupport)

+ (void)wk_registerScheme:(NSString*)scheme;

+ (void)wk_unregisterScheme:(NSString*)scheme;

@end
