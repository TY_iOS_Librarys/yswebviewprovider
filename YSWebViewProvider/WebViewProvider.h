//
//  WebViewProvider.h
//  PlayApp
//
//  Created by yans on 2016/12/23.
//  Copyright © 2016年 hzty. All rights reserved.
//

// Needed for UIViewController, UIWebViewDelegate, and UIView
#import <UIKit/UIKit.h>
// Needed for WKNavigationDelegate and WKUIDelegate
#import <WebKit/WebKit.h>
// Used to define the webView property below
#import "FLWebViewProvider.h"
#import "AXSecurityPolicy.h"
#import "UIWebView+JavascriptBridge.h"
#import "WKWebView+JavascriptBridge.h"
#import "WebViewProvider_InjectionJS.h"
#import "WebViewJavascriptBridgeDelegate.h"
#import "WebViewProviderSharedDelegate.h"
#import "WebViewSharedDelegate.h"
#import "WebViewProgressView.h"
#import "WebViewHelper.h"

#define WebViewProviderUseAutoLayout NO

static NSString *const kWP404NotFoundURLKey = @"wp_404_not_found";
static NSString *const kWPNetworkErrorURLKey = @"wp_network_error";

typedef void(^WKNavigationActionPolicyBlock)(WKNavigationActionPolicy);
typedef void(^WKNavigationResponsePolicyBlock)(WKNavigationResponsePolicy);
typedef NSURLSessionAuthChallengeDisposition (^WKWebViewDidReceiveAuthenticationChallengeHandler)(WKWebView *webView, NSURLAuthenticationChallenge *challenge, NSURLCredential * __autoreleasing *credential);


@interface WebViewProvider : NSObject<UIWebViewDelegate, WKNavigationDelegate, WKUIDelegate, WebViewProviderSharedDelegate>

// The main web view that is set up in the viewDidLoad method.
@property (nonatomic, strong) UIView<FLWebViewProvider,WebViewJavascriptBridgeDelegate,WebViewSharedDelegate> *webView;
@property (nonatomic, strong) WKWebViewConfiguration *config;

@property (nonatomic, copy) WKWebViewDidReceiveAuthenticationChallengeHandler challengeHandler;
@property (nonatomic, strong) AXSecurityPolicy *securityPolicy;



@property (nonatomic, strong) UILabel *backgroundLabel;
@property (nonatomic, assign) BOOL crossStatusBar;//是否穿过状态栏
@property (nonatomic, assign) BOOL registerHandlerWithInjectShort;//注入短方法
/// If is swiping now.
@property (nonatomic, assign)BOOL isSwipingBack;
#pragma mark - Shared Delegate Methods
@property (nonatomic, strong) UIButton *reloadBtn;
@property (nonatomic, assign) BOOL showReloadBtn;//是否显示点击刷新按钮
@property (nonatomic, assign) BOOL showLoadFailHtmlPage;//是否显示加载失败后HTML提示页
@property (nonatomic, assign) BOOL showLoadFailView;

@property (nonatomic, strong) UIView *loadFailView;                     // 加载失败view
@property (nonatomic, strong) UIImageView *loadFailRefreshImageView;    // 网络出错显示图标
@property (nonatomic, strong) UILabel *loadFailRefreshLabel;            // 网络出错显示文字
@property (nonatomic, strong) WebViewProgressView *webViewProgressView;


/// Max length of title string content. Default is 10.
@property (nonatomic, assign) NSUInteger maxAllowedTitleLength;
@property (nonatomic, copy) void(^injectJsCodeBlock)(id data, NSError *error);
@property (nonatomic, copy) BOOL(^shouldStartDecidePolicy)(NSURLRequest *);
@property (nonatomic, copy) void(^didStartNavigation)(void);
@property (nonatomic, copy) void(^didStartNavigationRequest)(NSURLRequest *);
@property (nonatomic, copy) void(^failLoadOrNavigation)(NSURLRequest *,NSError *);
@property (nonatomic, copy) void(^finishLoadOrNavigation)(NSURLRequest *);
@property (nonatomic, copy) void(^runJavaScriptPanelWithMessage)(NSString *,UIAlertController *);//2017.4.6

#pragma mark - 
@property (nonatomic, assign) NSTimeInterval timeoutInternal;/// Cache policy.
@property (nonatomic, assign) NSURLRequestCachePolicy cachePolicy;

@property (nonatomic, assign) BOOL showProgress;//default is no
@property (nonatomic, assign) BOOL useHtmlTitle;//default is no
@property (nonatomic, assign) BOOL useBackButton;//default is no
@property (nonatomic, strong) NSString *useBackButtonTitle;//default is 返回
@property (nonatomic, assign) BOOL useBackButtonTitleSpace;//default is yes 是否显示 返回按钮左边的空格占位符
@property (nonatomic, assign) BOOL rootShowBackButton;//default is yes
@property (nonatomic, assign) BOOL useBackCloseButton;//default is no
@property (nonatomic, strong) NSString *useBackCloseButtonTitle;//default is 关闭

@property (nonatomic, strong) UIImage *backIcon;//default is no
/// Open app link in app store app. Default is NO.
@property (nonatomic, assign) BOOL reviewsAppInAppStore;
@property (nonatomic, assign) BOOL showWVWindowErrors;//是否显示window js 错误日志到xcode控制台
@property (nonatomic, assign) BOOL isPresent;//是弹入还是推入

@property (nonatomic, strong, readonly) NSMutableArray *webViewConstraints;

@property (nonatomic, strong) NSURL *URL;

@property (nonatomic, strong) UIColor *progressTintColor;
@property (nonatomic, strong) UIColor *navigationBarTintColor;
@property (nonatomic, weak) UIViewController *targetController;

@property (nonatomic, copy) BOOL (^leftBtnBlock)(void);
@property (nonatomic, copy) void (^goBackBlock)(BOOL pop);
@property (nonatomic, copy) void (^closeBlock)(void);// default pop

- (instancetype)initAutoWithFrame:(CGRect)frame;

+ (instancetype)autoWithFrame:(CGRect)frame;
+ (instancetype)autoWithFrame:(CGRect)frame targetController:(UIViewController *)target;

- (void)addGoBackBlocks:(void(^)(BOOL))block;
- (void)addNameSpace:(NSString *)nameSpace;
- (void)addInjectJsCode:(NSString *)jsContent;
- (void)registerHandler:(NSString *)handlerName handler:(WVJBHandler)handler;
//页面onload中调用达成
- (NSString *)registerHandlerForOnloadCall:(NSString *)handlerName handler:(WVJBHandler)handler;
- (NSString *)registerHandlerForOnloadCall:(NSString *)handlerName nameSpace:(NSString *)nameSpace handler:(WVJBHandler)handler;

- (void)callHandler:(NSString *)handlerName data:(id)data responseCallback:(WVJBResponseCallback)responseCallback;
- (void)callSelfHandler:(NSString *)handlerName data:(id)data callback:(void(^)(id, NSError *))callback;

- (void)evaluateMethod:(NSString *)methodName data:(id)data completionHandler:(void (^)(id, NSError *))completionHandler;
- (void)evaluateJavaScript:(NSString *)javaScriptString completionHandler: (void (^)(id, NSError *)) completionHandler;

- (void)loadRequest:(NSURLRequest *)request;
- (void)loadURL:(NSURL *)url;
- (void)loadURLString:(NSString *)urlString;
- (void)loadHTMLString:(NSString *)string baseURL:(NSURL *)baseURL;

#if __IPHONE_OS_VERSION_MIN_REQUIRED < __IPHONE_8_0
- (void)pushCurrentSnapshotViewWithRequest:(NSURLRequest*)request;
#endif

- (void)reload;
- (void)goBack;
- (void)goForward;

- (void)stopLoading;

/**
 这个方法可以及时清理导航条进度
 一般界面后退的时候 同时调用:
 [xx stopLoading];
 [xx cleanWebView];
 */
- (void)cleanWebView;
- (void)stopPlayingMedia;
- (void)updateNavigationItems;//更新导航条按钮状态

+ (void)clearWebCacheCompletion:(dispatch_block_t)completion;
+ (void)applicationOpenURL:(NSURL *)url completionHandler:(void (^ __nullable)(BOOL success))completion;

- (BOOL)findProductInfoWithUrl:(NSString *)ursString;
@end
