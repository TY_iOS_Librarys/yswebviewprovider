//
//  main.m
//  YSWebViewProvider
//
//  Created by 478356182@qq.com on 03/01/2018.
//  Copyright (c) 2018 478356182@qq.com. All rights reserved.
//

@import UIKit;
#import "YSAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([YSAppDelegate class]));
    }
}
