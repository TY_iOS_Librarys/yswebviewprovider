//
//  YSViewController2.m
//  YSWebViewProvider_Example
//
//  Created by yans on 2018/11/8.
//  Copyright © 2018 478356182@qq.com. All rights reserved.
//

#import "YSViewController2.h"
#import "YSViewController.h"

@interface YSViewController2 ()

@end

@implementation YSViewController2

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeContactAdd];
    btn.frame = CGRectMake(100, 150, 50, 50);
    [btn addTarget:self action:@selector(goVc) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn];
    
    
}

- (void)goVc
{
    YSViewController *vc = [[YSViewController alloc] init];
    
    [self.navigationController pushViewController:vc animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
