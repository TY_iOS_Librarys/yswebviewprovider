//
//  YSViewController.m
//  YSWebViewProvider
//
//  Created by 478356182@qq.com on 03/01/2018.
//  Copyright (c) 2018 478356182@qq.com. All rights reserved.
//

#import "YSViewController.h"
#import "WebViewProvider.h"

@interface View :UIView
{
    
}

@end

@implementation View

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    NSLog(@"view - layoutSubviews");
}


@end

@interface YSViewController (){
    
}
    
@property (nonatomic, strong, readonly) WebViewProvider *webViewProvider;

@end

@implementation YSViewController

- (void)dealloc
{
    NSLog(@"... dealloc");
}

- (void)loadView
{
    self.view = [[View alloc] initWithFrame:[UIScreen mainScreen].bounds];
    
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    
    
//    self.edgesForExtendedLayout = UIRectEdgeAll;
//    self.extendedLayoutIncludesOpaqueBars = YES;//当Bar使用了不透明图片时，视图是否延伸至Bar所在区域
//    self.automaticallyAdjustsScrollViewInsets = NO;
//    self.navigationController.navigationBar.hidden = YES;
    
    
    CGRect webViewFrame = [self webViewFrame];
    _webViewProvider = [WebViewProvider autoWithFrame:webViewFrame];
    self.webViewProvider.targetController = self;
    self.webViewProvider.isSwipingBack = NO;
    self.webViewProvider.showReloadBtn = NO;
    self.webViewProvider.showLoadFailView = YES;
    [self.view addSubview:_webViewProvider.webView];
    
    
    self.webViewProvider.useHtmlTitle = YES;
    self.webViewProvider.useBackButton = YES;
    self.webViewProvider.useBackCloseButton = NO;
    self.webViewProvider.useBackButtonTitleSpace = YES;
    self.webViewProvider.useBackButtonTitle = @"返回";
    self.webViewProvider.backIcon = [UIImage imageNamed:@"klxt_header_back"];
    
    self.webViewProvider.progressTintColor = [UIColor redColor];
    self.webViewProvider.navigationBarTintColor = [UIColor redColor];
    
    self.webViewProvider.progressTintColor = [UIColor redColor];
    self.webViewProvider.backgroundLabel.hidden = YES;
    self.webViewProvider.showProgress = YES;
    self.webViewProvider.webView.scrollView.showsVerticalScrollIndicator = NO;
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor redColor]}];
    
    self.edgesForExtendedLayout = UIRectEdgeAll; // 布局是否自动延伸到四周
    self.extendedLayoutIncludesOpaqueBars = YES; // 当Bar使用了不透明图片时，视图是否延伸至Bar所在区域

    [[[UIApplication sharedApplication] keyWindow] addSubview:self.webViewProvider.webViewProgressView];
    
    [self registerBridge];
    
    [self registerAction];
    
//    [self.webViewProvider loadURLString:@"http://www.abc.com/khzy_api.html"];
//    [self.webViewProvider loadURLString:@"http://www.91118.com/StrangeWord/"];
//    [self.webViewProvider loadURLString:@"http://www.baidu.com"];
    
    
    
    NSURL *URL = [NSURL URLWithString:@"http://192.168.0.65:8555/klxt/a.aspx"];
    URL = [NSURL URLWithString:@"https://i.yd-jxt.com/englishreding/html5/index?ver=3.3.3"];
    NSMutableURLRequest *msreq = [NSMutableURLRequest requestWithURL:URL];
    [msreq setValue:@"action=goapp" forHTTPHeaderField:@"Cookie"];
    [self.webViewProvider loadRequest:msreq];
    

    
//    NSString *url = @"https://itunes.apple.com/cn/app/%E5%B8%88%E7%94%9F%E9%80%9A%E6%96%B0%E7%89%88/id1024378513?l=zh&ls=1&mt=8";
//    [self.webViewProvider loadURLString:url];

    self.automaticallyAdjustsScrollViewInsets = NO;
    

    
    return;
    
    
    NSString *htmlPath = [[NSBundle mainBundle] pathForResource:@"khzy_api" ofType:@"html"];
    NSString *htmlString = [NSString stringWithContentsOfFile:htmlPath encoding:NSUTF8StringEncoding error:nil];
    [self.webViewProvider loadHTMLString:htmlString baseURL:nil];

    NSMutableDictionary *paramDic = [NSMutableDictionary dictionary];

//    NSString *uu = @"http://www.abc.com/khzy_api.html?t";//t=3&a=2
//    if ([uu containsString:@"?"] && ![uu hasSuffix:@"?"]) {
//        NSRange range = [uu rangeOfString:@"?"];
//        NSUInteger location = range.location;
//        if (location != NSNotFound) {
//            NSString *host = [uu substringToIndex:location];
//            NSString *paramStr = [uu substringFromIndex:location+1];
//            NSArray *params = [paramStr componentsSeparatedByString:@"&"];
//            for (NSString *paramPartStr in params) {
//                NSArray *paramPart = [paramPartStr componentsSeparatedByString:@"="];
//                NSString *paramName = nil;
//                if (paramPart.count > 0) {
//                    paramName = [paramPart[0] stringByRemovingPercentEncoding];
//                }else{
//                    continue;
//                }
//                NSString *paramValue = nil;
//                if (paramPart.count > 1) {
//                    paramValue = [paramPart[1] stringByRemovingPercentEncoding];
//                }else{
//                    paramValue = @"";
//                }
//
//                [paramDic setObject:paramValue forKey:paramName];
//            }
//        }
//    }
    
    
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
 

}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

//    self.navigationController.navigationBar.translucent = NO;
//    [[[self.navigationController.navigationBar subviews] objectAtIndex:0] setAlpha:0];

    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];

}


- (void)registerBridge
{
    self.webViewProvider.registerHandlerWithInjectShort = YES;
    
//    [self.webViewProvider addInjectJsCode:generateGlobalNameSpace(@"nativeJS")];
//    [self.webViewProvider addInjectJsCode:generateGlobalNameSpace(@"WVApi")];
    [self.webViewProvider addNameSpace:@"WVApi"];
    
#pragma mark - sdk
    NSArray *registerHandlers = [self registerHandlers];
    if (registerHandlers) {
        for (NSString *registerHandler in registerHandlers) {
            SEL sel = NSSelectorFromString([NSString stringWithFormat:@"%@:callback:",registerHandler]);
            
            [self registerHandler:registerHandler
                          handler:^(id data, WVJBResponseCallback callback) {
                              if ([self respondsToSelector:sel]) {
                                  [self performSelector:sel withObject:data withObject:callback];
                              }
                          }];
        }
    }
}

- (void)registerHandler:(NSString *)handlerName handler:(WVJBHandler)handler
{
    //预注册
    [self.webViewProvider registerHandlerForOnloadCall:handlerName
                                  handler:handler];
    
//    [self.webViewProvider registerHandler:handlerName
//                                  handler:handler];
}

- (NSArray *)registerHandlers
{
    return @[
             @"testLog",
             @"testShare",
             ];
}

- (void)testLog:(NSDictionary *)data callback:(WVJBResponseCallback)callback
{
    NSLog(@"testLog -->>>>> %@",data);

    callback(@"abc");
}

- (void)testShare:(NSDictionary *)data callback:(WVJBResponseCallback)callback
{
    NSLog(@"testShare -->>>>> %@",data);
    
    callback(@"abc...");
}

- (void)registerAction
{
    [self.webViewProvider didStartNavigation:^{

    }];
    
    [self.webViewProvider setFinishLoadOrNavigation:^(NSURLRequest *request) {

    }];
    
    [self.webViewProvider setFailLoadOrNavigation:^(NSURLRequest *request, NSError *error) {

    }];
    
    [self.webViewProvider setRunJavaScriptPanelWithMessage:^(NSString *message, UIAlertController *alert) {

        
    }];
}

- (CGRect)webViewFrame
{
    CGFloat space = 44 + [[UIApplication sharedApplication] statusBarFrame].size.height;
    return CGRectMake(0, space, self.view.frame.size.width, self.view.frame.size.height-space);
//    return self.view.frame;
}
    
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
