#
# Be sure to run `pod lib lint YSWebViewProvider.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
s.name             = 'YSWebViewProvider'
s.version          = '1.1.22'
s.summary          = ' - '
s.license = { :type => 'MIT', :file => 'LICENSE' }
s.authors = {'yans' => 'aoyan2010@live.cn'}

#s.platform     = :ios, '7.0'
s.ios.deployment_target = '7.0'

s.homepage = 'https://bitbucket.org/tytto/yswebviewprovider.git'
s.source = {'git': 'https://bitbucket.org/tytto/yswebviewprovider.git',:tag => s.version.to_s}

s.source_files = 'YSWebViewProvider/**/*.{h,m}'

s.public_header_files = 'YSWebViewProvider/**/*.h'

s.resource  = 'YSWebViewProvider/Resources/WebView.bundle'
#s.resources = 'YSWebViewProvider/Resources/*.png'

s.frameworks = 'UIKit', 'Foundation', 'WebKit'
s.requires_arc = true

s.dependency 'WebViewJavascriptBridge'
s.dependency 'AXPracticalHUD'
s.dependency 'NJKWebViewProgress'

end




